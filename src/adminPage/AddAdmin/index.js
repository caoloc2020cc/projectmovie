import React, { useState } from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import { useDispatch, useSelector } from "react-redux";
import * as yup from "yup";
import "./../Dashboard/index.scss";
import {
  addAdmin,
  getAdmimBoth,
  getAdmimWithGP,
  getAdmimWithName,
  getAdmin,
  editActAdmin,
  deleteActAdmin,
} from "./module/action";
import Loader from "../../components/Loader/Loader";

export default function AddAdmin() {
  const [state, setState] = useState({
    taiKhoan: "",
    hoTen: "",
    email: "",
    soDt: "",
    maNhom: "",
    maLoaiNguoiDung: "",
    matKhau: "",
    xoaTaiKhoan: "",
  });
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.addADMINReducer.loading);
  const data = useSelector((state) => state.addADMINReducer.data);

  const error = useSelector((state) => state.addADMINReducer.error);

  //state get
  const loadingGET = useSelector((state) => state.addADMINReducer.loadingGET);
  const dataGET = useSelector((state) => state.addADMINReducer.dataGET);
  const errorGET = useSelector((state) => state.addADMINReducer.errorGET);
  //state edit
  const loadingEDIT = useSelector((state) => state.addADMINReducer.loadingEDIT);
  const dataEDIT = useSelector((state) => state.addADMINReducer.dataEDIT);
  const errorEDIT = useSelector((state) => state.addADMINReducer.errorEDIT);
  //state delete
  const loadingDELETE = useSelector(
    (state) => state.addADMINReducer.loadingDELETE
  );
  const dataDELETE = useSelector((state) => state.addADMINReducer.dataDELETE);
  const errorDELETE = useSelector((state) => state.addADMINReducer.errorDELETE);
  const signupAdminSchema = yup.object().shape({
    taiKhoan: yup.string().required("Nhập tài khoản"),
    hoTen: yup.string().required("Nhập họ tên"),
    matKhau: yup.string().required("Nhập mật khẩu"),
    email: yup.string().required("nhập email").email("email không hợp lệ"),
    soDt: yup
      .string()
      .required("nhập Số điện thoại đi")
      .matches(
        /([\+84|84|0]+(3|5|7|8|9|1[2|6|8|9]))+([0-9]{8})\b/,
        "số đt ko hợp lệ"
      ),
    maNhom: yup.string().required("CHọn mã nhóm đi"),
    maLoaiNguoiDung: yup.string().required("chọn mã loại người dùng đi"),
  });
  //submit add
  const handleSubmit = (value) => {
    dispatch(addAdmin(value));
  };
  //render noti
  const renderNoti = () => {
    return (
      (error && (
        <div className="alert alert-danger">{error.response.data}</div>
      )) ||
      (data && <div className="alert alert-success">Thêm thành công</div>) ||
      (loading && <Loader />)
    );
  };
  const renderNotiEdit = () => {
    console.log(errorEDIT, dataEDIT, loadingEDIT);

    return (
      (errorEDIT && (
        <div className="alert alert-danger">{errorEDIT.response.data}</div>
      )) ||
      (dataEDIT && (
        <div className="alert alert-success">Edit thành công</div>
      )) ||
      (loadingEDIT && <Loader />)
    );
  };
  const render = React.createRef();
  const renderDeleteNoti = () => {
    return (
      (errorDELETE && (
        <div className="alert alert-danger">{errorDELETE.response.data}</div>
      )) ||
      (dataDELETE && (
        <div className="alert alert-success">DELETE thành công</div>
      )) ||
      (loadingDELETE && <Loader />)
    );
  };
  // dispatch GET LIST Admin
  const handleGetListAdmin = (value) => {
    console.log(value);
    if (value.hoTen && value.maNhom) {
      dispatch(getAdmimBoth(value));
    } else if (value.hoTen || value.maNhom) {
      if (value.hoTen) {
        dispatch(getAdmimWithName(value));
      } else {
        dispatch(getAdmimWithGP(value));
      }
    } else {
      dispatch(getAdmin());
    }
  };
  const renderListGetAdmin = () => {
    return (
      dataGET &&
      dataGET.map((item, index) => {
        return (
          <tr key={index}>
            <td className="text-break col">{item.taiKhoan}</td>
            <td className="text-break col">{item.matKhau}</td>
            <td className="text-break col">{item.hoTen}</td>
            <td className="text-break col-two">{item.email}</td>
            <td className="text-break col">{item.soDt}</td>
            <td className="col text-break">{item.maLoaiNguoiDung}</td>
            <td className="col text-break">
              <button
                className="btn btn-success w-100 mb-1"
                onClick={() => editAdmin(item)}
              >
                Edit
              </button>
              <button
                className="btn btn-danger w-100"
                data-toggle="modal"
                data-target="#exampleModalCenter"
                onClick={() =>
                  setState({
                    ...state,
                    xoaTaiKhoan: item.taiKhoan,
                  })
                }
              >
                Delete
              </button>
            </td>
          </tr>
        );
      })
    );
  };
  // Edit
  const editAdmin = (item) => {
    setState({
      ...state,
      taiKhoan: item.taiKhoan,
      hoTen: item.hoTen,
      email: item.email,
      soDt: item.soDt,
      maNhom: "",
      maLoaiNguoiDung: item.maLoaiNguoiDung,
      matKhau: item.matKhau,
    });
  };
  const editSubmitAdmin = () => {
    console.log(state);

    dispatch(editActAdmin(state));
  };
  //delete
  const close = React.createRef();
  const deleteAdmin = () => {
    console.log(state.xoaTaiKhoan);
    dispatch(deleteActAdmin(state.xoaTaiKhoan));
    close.current.click();
  };

  return (
    <>
      <Formik
        initialValues={{
          taiKhoan: "",
          hoTen: "",
          email: "",
          soDt: "",
          maNhom: "",
          maLoaiNguoiDung: "",
          matKhau: "",
        }}
        validationSchema={signupAdminSchema}
        onSubmit={handleSubmit}
        render={(formikProps) => {
          console.log(formikProps);
          return (
            <Form className="container">
              <h1 className="text-center">Đăng Kí Tài Khoản</h1>
              <div className="form-group">
                <label htmlFor="1">Tai khoan</label>
                <Field
                  className="form-control"
                  id="1"
                  placeholder="Tai khoan"
                  name="taiKhoan"
                  onChange={(e) => {
                    formikProps.handleChange(e);

                    setState({
                      ...state,
                      taiKhoan: e.target.value,
                    });
                  }}
                  value={(formikProps.values.taiKhoan = state.taiKhoan)}
                />
                <ErrorMessage name="taiKhoan">
                  {(msg) => <div className="alert alert-danger">{msg}</div>}
                </ErrorMessage>
              </div>
              <div className="form-group">
                <label htmlFor="2">Password</label>
                <Field
                  type="password"
                  className="form-control"
                  id="2"
                  placeholder="Password"
                  name="matKhau"
                  onChange={(e) => {
                    formikProps.handleChange(e);

                    setState({
                      ...state,
                      matKhau: e.target.value,
                    });
                  }}
                  value={(formikProps.values.matKhau = state.matKhau)}
                />
                <ErrorMessage name="matKhau">
                  {(msg) => <div className="alert alert-danger">{msg}</div>}
                </ErrorMessage>
              </div>
              <div className="form-group">
                <label htmlFor="3">Hoten</label>
                <Field
                  className="form-control"
                  id="3"
                  placeholder="hoTen"
                  name="hoTen"
                  onChange={(e) => {
                    formikProps.handleChange(e);

                    setState({
                      ...state,
                      hoTen: e.target.value,
                    });
                  }}
                  value={(formikProps.values.hoTen = state.hoTen)}
                />
                <ErrorMessage name="hoTen">
                  {(msg) => <div className="alert alert-danger">{msg}</div>}
                </ErrorMessage>
              </div>
              <div className="form-group">
                <label htmlFor="4">email</label>
                <Field
                  type="email"
                  className="form-control"
                  id="4"
                  placeholder="email"
                  name="email"
                  onChange={(e) => {
                    formikProps.handleChange(e);

                    setState({
                      ...state,
                      email: e.target.value,
                    });
                  }}
                  value={(formikProps.values.email = state.email)}
                />
                <ErrorMessage name="email">
                  {(msg) => <div className="alert alert-danger">{msg}</div>}
                </ErrorMessage>
              </div>
              <div className="form-group">
                <label htmlFor="5">soDt</label>
                <Field
                  className="form-control"
                  id="5"
                  placeholder="soDt"
                  name="soDt"
                  onChange={(e) => {
                    formikProps.handleChange(e);

                    setState({
                      ...state,
                      soDt: e.target.value,
                    });
                  }}
                  value={(formikProps.values.soDt = state.soDt)}
                />
                <ErrorMessage name="soDt">
                  {(msg) => <div className="alert alert-danger">{msg}</div>}
                </ErrorMessage>
              </div>
              <div className="form-group">
                <label htmlFor="6">maNhom</label>
                <Field
                  component="select"
                  className="form-control"
                  id="6"
                  name="maNhom"
                  onChange={(e) => {
                    formikProps.handleChange(e);

                    setState({
                      ...state,
                      maNhom: e.target.value,
                    });
                  }}
                  value={(formikProps.values.maNhom = state.maNhom)}
                >
                  <option></option>
                  <option>GP01</option>
                  <option>GP02</option>
                  <option>GP03</option>
                  <option>GP04</option>
                  <option>GP05</option>
                  <option>GP06</option>
                  <option>GP07</option>
                  <option>GP08</option>
                  <option>GP09</option>
                  <option>GP010</option>
                </Field>
                <ErrorMessage name="maNhom">
                  {(msg) => <div className="alert alert-danger">{msg}</div>}
                </ErrorMessage>
              </div>
              <div className="form-group">
                <label htmlFor="7">Mã Loại Người dùng</label>
                <Field
                  component="select"
                  className="form-control"
                  id="7"
                  name="maLoaiNguoiDung"
                  onChange={(e) => {
                    formikProps.handleChange(e);

                    setState({
                      ...state,
                      maLoaiNguoiDung: e.target.value,
                    });
                  }}
                  value={
                    (formikProps.values.maLoaiNguoiDung = state.maLoaiNguoiDung)
                  }
                >
                  <option></option>
                  <option>QuanTri</option>
                  <option>KhachHang</option>
                </Field>
                <ErrorMessage name="maLoaiNguoiDung">
                  {(msg) => <div className="alert alert-danger">{msg}</div>}
                </ErrorMessage>
              </div>
              {renderNoti()}
              <div className="text-center mb-1">
                <button type="submit" className="btn btn-primary mr-2 ">
                  Submit
                </button>
              </div>
            </Form>
          );
        }}
      />
      <div className="text-center mb-5">
        <div>{renderNotiEdit()}</div>
        <button className="btn btn-danger" onClick={editSubmitAdmin}>
          Cập nhật
        </button>
      </div>

      <div>
        <h1>Danh sách người dùng</h1>
      </div>
      <Formik
        initialValues={{
          hoTen: "",
          maNhom: "",
        }}
        onSubmit={handleGetListAdmin}
        render={(formikprops) => {
          return (
            <Form className="d-flex justify-content-around align-items-center">
              <div className="form-group">
                <label>Tên</label>
                <Field
                  className="form-control mt-2"
                  name="hoTen"
                  onChange={formikprops.handleChange}
                />
              </div>

              <div className="form-group ml-1 w-50">
                <label>Mã nhóm</label>
                <Field
                  className="form-control mt-2"
                  name="maNhom"
                  onChange={formikprops.handleChange}
                />
              </div>
              <button
                className="btn btn-success h-50 mt-4"
                type="submit"
                ref={render}
              >
                Tìm Kiếm
              </button>
            </Form>
          );
        }}
      />

      <div className="container">
        <div
          className="modal fade"
          id="exampleModalCenter"
          tabIndex={-1}
          role="dialog"
          aria-labelledby="exampleModalCenterTitle"
          aria-hidden="true"
        >
          <div className="modal-dialog modal-dialog-centered" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="exampleModalLongTitle">
                  Bạn muốn xóa tài khoản {state.xoaTaiKhoan}?
                </h5>
                <button
                  type="button"
                  className="close"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">×</span>
                </button>
              </div>

              <div className="modal-footer">
                <button
                  type="button"
                  className="btn btn-secondary"
                  data-dismiss="modal"
                  ref={close}
                >
                  Close
                </button>
                <button
                  type="button"
                  className="btn btn-primary"
                  onClick={deleteAdmin}
                >
                  Xóa lun
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
      {renderDeleteNoti()}
      <div className="container">
        <div className="row m-0">
          <table class="col-lg-12 table-bordered table-striped table-condensed table-fixed">
            <thead>
              <tr className="d-flex">
                <th className="col d-block">Tài Khoản</th>
                <th className="col d-block">Password</th>
                <th className="col d-block">Họ tên</th>
                <th className="col-two d-block">Email</th>
                <th className="col d-block">Số DT</th>

                <th className="col d-block">Mã loại</th>
                <th className="col d-block">Thao Tác</th>
              </tr>
            </thead>

            <tbody>{renderListGetAdmin()}</tbody>
          </table>
        </div>
      </div>
    </>
  );
}
