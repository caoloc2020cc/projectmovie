import * as ActionType from "./constants.js";

let initialState = {
  loading: false,
  data: null,
  error: null,
  loadingGET: false,
  dataGET: null,
  errorGET: null,
  userEdit: null,
  loadingEDIT: false,
  dataEDIT: null,
  errorEDIT: null,
  loadingDELETE: false,
  dataDELETE: null,
  errorDELETE: null,
  loadingEDITlich: false,
  dataEDITlich: null,
  errorEDITlich: null,
};
const quanliMovieReducer = (state = initialState, action) => {
  switch (action.type) {
    case ActionType.ADDREQUEST_MOVIE:
      state.loading = true;
      state.data = null;
      state.error = null;
      return { ...state };
    case ActionType.ADDFAILED_MOVIE:
      state.loading = false;
      state.data = null;
      state.error = action.payload;
      return { ...state };
    case ActionType.ADDSUCCESS_MOVIE:
      state.loading = false;
      state.data = action.payload;
      state.error = null;
      return { ...state };
    case ActionType.GETREQUEST_MOVIE:
      state.loadingGET = true;
      state.dataGET = null;
      state.errorGET = null;
      return { ...state };
    case ActionType.GETFAILED_MOVIE:
      state.loadingGET = false;
      state.dataGET = null;
      state.errorGET = action.payload;
      return { ...state };
    case ActionType.GETSUCCESS_MOVIE:
      state.loadingGET = false;
      state.dataGET = action.payload;
      state.errorGET = null;
      return { ...state };
    //case edit
    case ActionType.EDIT_MOVIE:
      state.userEdit = action.payload;
      return { ...state };

    //edit submit
    case ActionType.EDITREQUEST_MOVIE:
      state.loadingEDIT = true;
      state.dataEDIT = null;
      state.errorEDIT = null;
      return { ...state };
    case ActionType.EDITFAILED_MOVIE:
      state.loadingEDIT = false;
      state.dataEDIT = null;
      state.errorEDIT = action.payload;
      return { ...state };
    case ActionType.EDITSUCCESS_MOVIE:
      state.loadingEDIT = false;
      state.dataEDIT = action.payload;
      state.errorEDIT = null;
      return { ...state };
    case ActionType.DELETEREQUEST_MOVIE:
      state.loadingDELETE = true;
      state.dataDELETE = null;
      state.errorDELETE = null;
      return { ...state };
    case ActionType.DELETEFAILED_MOVIE:
      state.loadingDELETE = false;
      state.dataDELETE = null;
      state.errorDELETE = action.payload;
      return { ...state };
    case ActionType.DELETESUCCESS_MOVIE:
      state.loadingDELETE = false;
      state.dataDELETE = action.payload;
      state.errorDELETE = null;
      console.log(action.payload, state.dataGET);
      state.dataGET.map((item, index) => {
        if (item.maPhim === action.maPhim) {
          console.log("xóa");
          state.dataGET.splice(index, 1);
        }
      });
      return { ...state };
    case ActionType.EDITREQUEST_LichChieu:
      state.loadingEDITlich = true;
      state.dataEDITlich = null;
      state.errorEDITlich = null;
      return { ...state };
    case ActionType.EDITFAILED_LichChieu:
      state.loadingEDITlich = false;
      state.dataEDITlich = null;
      state.errorEDITlich = action.payload;
      return { ...state };
    case ActionType.EDITSUCCESS_LichChieu:
      state.loadingEDITlich = false;
      state.dataEDITlich = action.payload;
      state.errorEDITlich = null;
      return { ...state };
    default:
      return { ...state };
  }
};
export default quanliMovieReducer;
