import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import Loader from "../../components/Loader/Loader";

import {
  getActFilmBoth,
  getActFilmGP,
  getActFilmTenPhim,
  getActFilmNone,
  editActFilm,
  deleteFlimAct,
} from "./module/action";
import TaoLichChieu from "./TaoLichChieu";
import "./../Dashboard/index.scss";
export default function FindFlim() {
  //     state
  const [state, setState] = useState({
    tenPhim: "",
    maNhom: "",
    xoaMaPhim: "",
    xoaTenPhim: "",
    taoLich: "",
  });
  // props
  const dataGET = useSelector((state) => state.quanliMovieReducer.dataGET);
  const errorGET = useSelector((state) => state.quanliMovieReducer.errorGET);
  const loadingGET = useSelector(
    (state) => state.quanliMovieReducer.loadingGET
  );
  const loadingDELETE = useSelector(
    (state) => state.quanliMovieReducer.loadingDELETE
  );
  const dataDELETE = useSelector(
    (state) => state.quanliMovieReducer.dataDELETE
  );
  const errorDELETE = useSelector(
    (state) => state.quanliMovieReducer.errorDELETE
  );
  //  dispatch
  const dispatch = useDispatch();
  // HandleOnchange
  const handleOnChange = (e) => {
    const { value, name } = e.target;
    console.log(name, value);
    setState({
      ...state,
      [name]: value,
    });
  };
  // Submit
  const handleOnSubmit = (e) => {
    e.preventDefault();
    const { tenPhim, maNhom } = state;
    if (tenPhim && maNhom) {
      dispatch(getActFilmBoth(state));
    } else if (tenPhim || maNhom) {
      if (tenPhim) {
        dispatch(getActFilmTenPhim(state));
      } else {
        dispatch(getActFilmGP(state));
      }
    } else {
      dispatch(getActFilmNone(state));
    }
  };
  //xóa phim
  const close = React.createRef();
  const deleteFilm = () => {
    dispatch(deleteFlimAct(state.xoaMaPhim));
    close.current.click();
  };
  //Render xóa phim
  const renderDeleteNoti = () => {
    console.log(dataDELETE);

    return (
      (errorDELETE && (
        <div className="alert alert-danger">{errorDELETE.response.data}</div>
      )) ||
      (dataDELETE && (
        <div className="alert alert-success">DELETE thành công</div>
      )) ||
      (loadingDELETE && <Loader />)
    );
  };

  // Render LIst Movie

  const renderListMovie = () => {
    return (
      dataGET &&
      dataGET.map((item, index) => {
        return (
          <tr key={index}>
            <td>{item.maPhim}</td>
            <td className="text-break col">{item.tenPhim}</td>
            <td className="text-break col">{item.biDanh}</td>
            <td className="text-break col">{item.trailer}</td>
            <td className="text-break col">{item.hinhAnh}</td>
            <td className="text-break col">{item.moTa}</td>

            <td className="text-break col">{item.ngayKhoiChieu}</td>

            <td className="text-break col">
              <button
                className="btn btn-success"
                onClick={() => editFilm(item)}
              >
                Edit
              </button>
              <button
                className="btn btn-danger w-100"
                data-toggle="modal"
                data-target="#exampleModalCenter"
                onClick={() =>
                  setState({
                    ...state,
                    xoaMaPhim: item.maPhim,
                    xoaTenPhim: item.tenPhim,
                  })
                }
              >
                Delete
              </button>
              <button
                className="btn btn-primary w-100"
                data-toggle="modal"
                data-target="#createLichChieu"
                onClick={() =>
                  setState({
                    ...state,
                    taoLich: item.maPhim,
                  })
                }
              >
                Tạo Lịch chiếu
              </button>
            </td>
          </tr>
        );
      })
    );
  };

  // EDIT
  const editFilm = (item) => {
    item.ngayKhoiChieu = item.ngayKhoiChieu.slice(0, 10);
    let year = item.ngayKhoiChieu.slice(0, 4);
    let month = item.ngayKhoiChieu.slice(4, 8);
    let day = item.ngayKhoiChieu.slice(8, 10);
    item.ngayKhoiChieu = day.concat(month, year);
    console.log(item);
    dispatch(editActFilm(item));
  };
  return (
    <div>
      <form
        onSubmit={handleOnSubmit}
        className="container d-flex align-items-center justify-content-between"
      >
        <div className="form-group w-50">
          <label htmlFor="tenPhim">Tên Phim</label>
          <input
            id="tenPhim"
            name="tenPhim"
            className="form-control"
            onChange={handleOnChange}
          />
        </div>
        <div className="form-group w-30">
          <label htmlFor="Manhom">Mã Nhóm</label>
          <select
            id="Manhom"
            name="maNhom"
            className="form-control"
            onChange={handleOnChange}
          >
            <option></option>
            <option>GP01</option>
            <option>GP02</option>
            <option>GP03</option>
            <option>GP04</option>
            <option>GP05</option>
            <option>GP06</option>
            <option>GP07</option>
            <option>GP08</option>
            <option>GP09</option>
            <option>GP010</option>
          </select>
        </div>
        <div>
          <button type="submit" className="btn btn-success mt-3">
            Tìm
          </button>
        </div>
      </form>
      <div>
        <div
          className="modal fade"
          id="exampleModalCenter"
          tabIndex={-1}
          role="dialog"
          aria-labelledby="exampleModalCenterTitle"
          aria-hidden="true"
        >
          <div className="modal-dialog modal-dialog-centered" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="exampleModalLongTitle">
                  Bạn muốn xóa phim: {state.xoaTenPhim}?
                </h5>
                <button
                  type="button"
                  className="close"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">×</span>
                </button>
              </div>

              <div className="modal-footer">
                <button
                  type="button"
                  className="btn btn-secondary"
                  data-dismiss="modal"
                  ref={close}
                >
                  Close
                </button>
                <button
                  type="button"
                  className="btn btn-primary"
                  onClick={deleteFilm}
                >
                  Xóa lun
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
      {renderDeleteNoti()}

      <table className="table_10 col-lg-12 table-bordered table-striped table-condensed table-fixed">
        <thead>
          <tr>
            <th className="col">Mã Phim</th>
            <th className="col">Tên Phim</th>
            <th className="col">Bí Danh</th>
            <th className="col">Trailer</th>
            <th className="col">Hình Ảnh</th>
            <th className="col">Mô tả</th>

            <th className="col">Ngày Khởi Chiếu</th>

            <th className="col">Thao tác</th>
          </tr>
        </thead>
        <tbody>{renderListMovie()}</tbody>
      </table>
      <TaoLichChieu maPhim={state.taoLich} />
    </div>
  );
}
