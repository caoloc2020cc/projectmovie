import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import Loader from "../../components/Loader/Loader";
import { addActFilm, editSubmitAct } from "./module/action";
export default function AddPhim() {
  const dispatch = useDispatch();
  //props
  const error = useSelector((state) => state.quanliMovieReducer.error);
  const data = useSelector((state) => state.quanliMovieReducer.data);
  const loading = useSelector((state) => state.quanliMovieReducer.loading);
  const userEdit = useSelector((state) => state.quanliMovieReducer.userEdit);
  const errorEDIT = useSelector((state) => state.quanliMovieReducer.errorEDIT);
  const dataEDIT = useSelector((state) => state.quanliMovieReducer.dataEDIT);
  const loadingEDIT = useSelector(
    (state) => state.quanliMovieReducer.loadingEDIT
  );
  const [state, setState] = useState({
    values: {
      maPhim: 0,
      biDanh: "",
      trailer: "",
      hinhAnh: "",
      tenPhim: "",
      maNhom: "",
      moTa: "",
      ngayKhoiChieu: "",
      danhGia: 0,
    },
    error: {
      maPhim: 0,
      biDanh: "",
      trailer: "",
      hinhAnh: "",
      moTa: "",
      maNhom: "",
      tenPhim: "",
      ngayKhoiChieu: "",
      danhGia: 0,
    },
    formValid: false,
    biDanhValid: false,
    trailerValid: false,
    hinhAnhValid: false,
    tenPhimValid: false,
    maNhomValid: false,
    moTaValid: false,
    ngayKhoiChieuValid: false,
  });
  // will receive props
  useEffect(() => {
    console.log("Prop Received: ", userEdit);
    if (userEdit) {
      setStateEdit(userEdit);

      console.log(state);
    }
  }, [userEdit]);
  const setStateEdit = (userEdit) => {
    setState({
      ...state,
      values: userEdit,
    });
  };
  //EDIT
  const editSubmit = (e) => {
    e.preventDefault();
    console.log(state);
    dispatch(editSubmitAct(state.values));
  };
  const handleOnChange = (e) => {
    const { name, value } = e.target;
    setState({
      ...state,
      values: { ...state.values, [name]: value },
    });
    console.log(state);
  };
  //ERROR
  const handleError = (e) => {
    const { name, value } = e.target;
    let mess = value === "" ? `${name} không được rỗng` : "";

    let {
      biDanhValid,
      trailerValid,
      hinhAnhValid,
      tenPhimValid,
      maNhomValid,
      moTaValid,
      ngayKhoiChieuValid,
    } = state;
    switch (name) {
      case "biDanh":
        biDanhValid = mess === "" ? true : false;
        break;
      case "trailer":
        trailerValid = mess === "" ? true : false;
        if (
          value &&
          !(value.indexOf("https://") == 0 || value.indexOf("http://") == 0)
        ) {
          mess = "Địa chỉ hình ảnh phải bắt đầu bằng http:// hoặc https:// ";
          trailerValid = false;
        }
        break;

      case "hinhAnh":
        hinhAnhValid = mess === "" ? true : false;

        if (
          value &&
          !(value.indexOf("https://") == 0 || value.indexOf("http://") == 0)
        ) {
          mess = "Địa chỉ hình ảnh phải bắt đầu bằng http:// hoặc https:// ";
          hinhAnhValid = false;
        }

        break;
      case "moTa":
        moTaValid = mess === "" ? true : false;

        break;
      case "maNhom":
        maNhomValid = mess === "" ? true : false;
        break;
      case "tenPhim":
        tenPhimValid = mess === "" ? true : false;
        break;
      case "ngayKhoiChieu":
        ngayKhoiChieuValid = mess === "" ? true : false;
        //(0[1-9]|[1-2][0-9]|3[0-1]),[0-9]{4}
        if (
          value &&
          !value.match(/(0[1-9]|[1-2][0-9]|3[0-1])-(0[1-9]|1[0-2])-[0-9]{4}/g)
        ) {
          mess = "Ngày khởi chiếu phải đúng định dạng dd-MM-yyyy";
          ngayKhoiChieuValid = false;
        }
        break;

      default:
        break;
    }
    console.log(name, mess);
    formValidation(
      name,
      mess,
      biDanhValid,
      trailerValid,
      hinhAnhValid,
      tenPhimValid,
      maNhomValid,
      moTaValid,
      ngayKhoiChieuValid
    );

    console.log(state);
  };
  // form  Validation
  const formValidation = (
    name,
    mess,
    biDanhValid,
    trailerValid,
    hinhAnhValid,
    tenPhimValid,
    maNhomValid,
    moTaValid,
    ngayKhoiChieuValid
  ) => {
    setState({
      ...state,
      error: { ...state.error, [name]: mess },
      biDanhValid,
      trailerValid,
      hinhAnhValid,
      tenPhimValid,
      maNhomValid,
      moTaValid,
      ngayKhoiChieuValid,
      formValid:
        biDanhValid &&
        trailerValid &&
        hinhAnhValid &&
        tenPhimValid &&
        maNhomValid &&
        moTaValid &&
        ngayKhoiChieuValid,
    });
  };
  //Submit
  const handleOnSubmit = (e) => {
    e.preventDefault();
    console.log(state.values);
    dispatch(addActFilm(state.values));
  };
  //Render Noti
  const renderNoti = () => {
    return (
      (error && (
        <div className="alert alert-danger">{error.response.data}</div>
      )) ||
      (data && <div className="alert alert-success">Thêm thành công</div>) ||
      (loading && <Loader />)
    );
  };
  //Render NotiEDIT
  const renderNotiEdit = () => {
    return (
      (errorEDIT && (
        <div className="alert alert-danger">{errorEDIT.response.data}</div>
      )) ||
      (dataEDIT && (
        <div className="alert alert-success">Cập nhật thành công</div>
      )) ||
      (loadingEDIT && <Loader />)
    );
  };
  return (
    <form className="container">
      <h1 className="">Quản lí Phim</h1>

      <div className="form-group">
        <label htmlFor="tenPhim">Tên phim</label>
        <input
          id="tenPhim"
          type="text"
          name="tenPhim"
          className="form-control"
          onChange={handleOnChange}
          onBlur={handleError}
          onKeyUp={handleError}
          value={state.values.tenPhim}
        />
        {state.error.tenPhim && (
          <div className="alert alert-danger mt-1">{state.error.tenPhim}</div>
        )}
      </div>
      <div className="form-group">
        <label htmlFor="biDanh">Bí danh</label>
        <input
          id="biDanh"
          type="text"
          name="biDanh"
          className="form-control"
          onChange={handleOnChange}
          onBlur={handleError}
          onKeyUp={handleError}
          value={state.values.biDanh}
        />
        {state.error.biDanh && (
          <div className="alert alert-danger mt-1">{state.error.biDanh}</div>
        )}
      </div>

      <div className="form-group">
        <label htmlFor="trailer">Trailer</label>
        <input
          id="trailer"
          type="text"
          name="trailer"
          className="form-control"
          onChange={handleOnChange}
          onBlur={handleError}
          onKeyUp={handleError}
          value={state.values.trailer}
        />
        {state.error.trailer && (
          <div className="alert alert-danger mt-1">{state.error.trailer}</div>
        )}
      </div>
      <div className="form-group">
        <label htmlFor="hinhAnh">Hình ảnh</label>
        <input
          id="hinhAnh"
          type="text"
          name="hinhAnh"
          className="form-control"
          onChange={handleOnChange}
          onBlur={handleError}
          onKeyUp={handleError}
          value={state.values.hinhAnh}
        />
        {state.error.honhAnh && (
          <div className="alert alert-danger mt-1">{state.error.hinhAnh}</div>
        )}
      </div>
      <div className="form-group">
        <label htmlFor="moTa">Mô tả</label>
        <input
          id="moTa"
          type="text"
          name="moTa"
          className="form-control"
          onChange={handleOnChange}
          onBlur={handleError}
          onKeyUp={handleError}
          value={state.values.moTa}
        />
        {state.error.moTa && (
          <div className="alert alert-danger mt-1">{state.error.moTa}</div>
        )}
      </div>
      <div className="form-group">
        <label htmlFor="maNhom">Mã nhóm</label>
        <select
          id="maNhom"
          type="text"
          name="maNhom"
          className="form-control"
          onChange={handleOnChange}
          onBlur={handleError}
          onKeyUp={handleError}
          value={state.values.maNhom}
        >
          <option></option>
          <option>GP01</option>
          <option>GP02</option>
          <option>GP03</option>
          <option>GP04</option>
          <option>GP05</option>
          <option>GP06</option>
          <option>GP07</option>
          <option>GP08</option>
          <option>GP09</option>
          <option>GP010</option>
        </select>
        {state.error.maNhom && (
          <div className="alert alert-danger mt-1">{state.error.maNhom}</div>
        )}
      </div>
      <div className="form-group">
        <label htmlFor="ngayKhoiChieu">Ngày khởi chiếu</label>
        <input
          id="ngayKhoiChieu"
          type="text"
          name="ngayKhoiChieu"
          className="form-control"
          onChange={handleOnChange}
          onBlur={handleError}
          onKeyUp={handleError}
          value={state.values.ngayKhoiChieu}
        />
        {state.error.ngayKhoiChieu && (
          <div className="alert alert-danger mt-1">
            {state.error.ngayKhoiChieu}
          </div>
        )}
      </div>
      {renderNoti()}
      {renderNotiEdit()}
      <div>
        <button
          type="submit"
          className="btn btn-success"
          disabled={!state.formValid}
          onClick={(e) => handleOnSubmit(e)}
        >
          Thêm
        </button>
        <button className="btn btn-danger" onClick={(e) => editSubmit(e)}>
          Cập nhật
        </button>
      </div>
    </form>
  );
}
