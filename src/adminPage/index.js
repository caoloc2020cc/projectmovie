import React from "react";
import { Route, Redirect } from "react-router-dom";
import NavbarAdmin from "../components/AdminComponent/NavBar/NavbarAmin";

function Layout(props) {
  return (
    <div className="wrapper">
      <NavbarAdmin />

      <div className="main-panel">{props.children}</div>
    </div>
  );
}
export default function AdminTemplate({ Component, ...props }) {
  return (
    <Route
      {...props}
      render={(propsComponent) => {
        if (localStorage.getItem("admin")) {
          return (
            <Layout>
              <Component {...propsComponent} />
            </Layout>
          );
        }
        return <Redirect to="/admin/auth" />;
      }}
    />
  );
}
