import * as ActionType from "./constants";
let initialState = {
  loadingUser: false,
  dataUser: null,
  errorUser: null,
  doanhThu: 0,
  objectName: [],
  objectLsDatVe: [],
};
const quanliDashboardReducer = (state = initialState, action) => {
  switch (action.type) {
    case ActionType.GETDASHBOARDUSER_REQUEST:
      state.loadingUser = true;
      state.dataUser = null;
      state.errorUser = null;
      return { ...state };
    case ActionType.GETDASHBOARDUSER_SUCCESS:
      state.loadingUser = false;
      state.dataUser = action.payload;
      state.errorUser = null;
      return { ...state };
    case ActionType.GETDASHBOARDUSER_FAILED:
      state.loadingUser = false;
      state.dataUser = null;
      state.errorUser = action.payload;
      return { ...state };
    case ActionType.GETDASHBOARDPROFIT_SUCCESS:
      let tong = 0;

      action.payload.thongTinDatVe.forEach((items) => {
        let a = items.giaVe * items.danhSachGhe.length;
        tong += a;

        console.log(items.ngayDat.slice(10, items.ngayDat.length));
        // let ngayDatModi = ;
        let objectDatve = {
          hoTen: action.payload.hoTen,
          taiKhoan: action.payload.taiKhoan,
          tenPhim: items.tenPhim,
          ngayGio:
            items.ngayDat.slice(0, 10) +
            " " +
            items.ngayDat.slice(11, items.ngayDat.length),
          soGhe: items.danhSachGhe.length,
          tenCumRap: items.danhSachGhe[0].tenHeThongRap,
          tenRap: items.danhSachGhe[0].tenRap,
        };
        state.objectLsDatVe.push(objectDatve);
        console.log(state.objectLsDatVe);
        console.log(objectDatve);
        let objectMini = { tenPhim: items.tenPhim, doanhThu: a };
        if (state.objectName.length === 0) {
          state.objectName.push(objectMini);
        } else {
          for (let i = 0; i <= state.objectName.length - 1; i++) {
            if (state.objectName[i].tenPhim === objectMini.tenPhim) {
              let object = {
                tenPhim: state.objectName[i].tenPhim,
                doanhThu: state.objectName[i].doanhThu + a,
              };

              state.objectName.fill(object, i, i + 1);
              break;
            } else if (
              state.objectName[i] !== objectMini.tenPhim &&
              i === state.objectName.length - 1
            ) {
              state.objectName.push(objectMini);

              break;
            }
          }
        }
      });

      state.doanhThu = state.doanhThu + tong;

      return { ...state };
    case ActionType.CLEAR:
      console.log("clear");
      state.dataUser = null;
      state.doanhThu = 0;
      state.objectName = [];
      state.objectLsDatVe = [];
      return { ...state };
    default:
      return { ...state };
  }
};
export default quanliDashboardReducer;
