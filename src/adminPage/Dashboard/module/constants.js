export const GETDASHBOARDUSER_FAILED =
  "@quanliDashboardReducer/GETDASHBOARDUSERFailed";
export const GETDASHBOARDUSER_REQUEST =
  "@quanliDashboardReducer/GETDASHBOARDUSERRequest";
export const GETDASHBOARDUSER_SUCCESS =
  "@quanliDashboardReducer/GETDASHBOARDUSERSuccess";
export const GETDASHBOARDPROFIT_FAILED =
  "@quanliDashboardReducer/GETDASHBOARDPROFITFailed";

export const GETDASHBOARDPROFIT_SUCCESS =
  "@quanliDashboardReducer/GETDASHBOARDPROFITSuccess";
export const CLEAR = "@quanliDashboardReducer/CLEAR";
