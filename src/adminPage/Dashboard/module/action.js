import axios from "axios";
import * as ActionType from "./constants";
export const getUserAct = () => {
  return (dispatch) => {
    axios({
      url: "https://movie0706.cybersoft.edu.vn/api/QuanLyNguoiDung/LayDanhSachNguoiDung?MaNhom=GP09",
      method: "GET",
    })
      .then((res) => {
        console.log(res);
        dispatch(actGetUserSuccess(res.data));
      })
      .catch((err) => {
        console.log(err);
        dispatch(actGetUserFailed(err));
      });
  };
};
export const getProfit = (taiKhoan) => {
  return (dispatch) => {
    axios({
      url: "https://movie0706.cybersoft.edu.vn/api/QuanLyNguoiDung/ThongTinTaiKhoan",
      method: "POST",
      data: taiKhoan,
    })
      .then((res) => {
        if (res.data.thongTinDatVe.length !== 0) {
          console.log("tien", res.data, res.data.thongTinDatVe.length);
          dispatch(actGetPROFITSuccess(res.data));
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };
};
export const clearAct = () => {
  return (dispatch) => {
    dispatch(actCLEAR());
  };
};
const actGetUserRequest = () => {
  return {
    type: ActionType.GETDASHBOARDUSER_REQUEST,
  };
};
const actGetUserSuccess = (data) => {
  return {
    type: ActionType.GETDASHBOARDUSER_SUCCESS,
    payload: data,
  };
};
const actGetUserFailed = (error) => {
  return {
    type: ActionType.GETDASHBOARDUSER_SUCCESS,
    payload: error,
  };
};
const actGetPROFITSuccess = (data) => {
  return {
    type: ActionType.GETDASHBOARDPROFIT_SUCCESS,
    payload: data,
  };
};
const actGetPROFITFailed = (error) => {
  return {
    type: ActionType.GETDASHBOARDPROFIT_SUCCESS,
    payload: error,
  };
};
const actCLEAR = () => {
  return {
    type: ActionType.CLEAR,
  };
};
