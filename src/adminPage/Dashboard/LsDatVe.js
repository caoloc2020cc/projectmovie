import React, { useState } from "react";
import { useEffect } from "react";
import { useSelector } from "react-redux";

export default function LsDatVe() {
  const [state, setState] = useState({
    lsDatVe: null,
  });
  const objectLsDatVe = useSelector(
    (state) => state.quanliDashboardReducer.objectLsDatVe
  );
  useEffect(() => {
    setState({
      ...state,
      lsDatVe: objectLsDatVe,
    });
    console.log("set state cho", state);
  }, [objectLsDatVe]);
  const renderTableLsDatve = () => {
    return (
      state.lsDatVe &&
      state.lsDatVe.map((item, index) => {
        return (
          <tr key={index}>
            <td className="col text-break">{item.taiKhoan}</td>
            <td className="col text-break">{item.hoTen}</td>
            <td className="col-two text-break">{item.ngayGio}</td>
            <td className="col text-break">{item.tenPhim}</td>
            <td className="col text-break">{item.soGhe}</td>
            <td className="col text-break">{item.tenCumRap}</td>
            <td className="col text-break">{item.tenRap}</td>
          </tr>
        );
      })
    );
  };
  const close = React.createRef();
  const ganDayNhat = () => {
    if (objectLsDatVe !== []) {
      let arraySapXep = objectLsDatVe.sort(function (a, b) {
        return new Date(b.ngayGio) - new Date(a.ngayGio);
      });
      console.log(arraySapXep);
      setState({
        ...state,
        lsDatVe: arraySapXep,
      });
      close.current.click();
    }
  };
  const lanXaNhat = () => {
    if (objectLsDatVe !== []) {
      let arraySapXep = objectLsDatVe.sort(function (a, b) {
        return new Date(a.ngayGio) - new Date(b.ngayGio);
      });
      console.log(arraySapXep);
      setState({
        ...state,
        lsDatVe: arraySapXep,
      });
      close.current.click();
    }
  };
  return (
    <div className="container">
      <div className="row m-0">
        <table className="col-lg-12 table-bordered table-striped table-condensed table-fixed">
          <thead>
            <tr>
              <th className="col">Tài Khoản</th>
              <th className="col">Họ Tên</th>
              <th className="col-two">
                <div className="position-relative">
                  <p className="p-0 m-0 text-center">
                    <a
                      data-toggle="collapse"
                      href="#collapseExample1"
                      role="button"
                      aria-expanded="false"
                      aria-controls="collapseExample"
                      className="text-white"
                      ref={close}
                    >
                      Ngày giờ đặt
                      <i class="fa fa-angle-down pl-1"></i>
                    </a>
                  </p>
                  <div
                    className="collapse position-absolute"
                    id="collapseExample1"
                  >
                    <div className="card card-body">
                      <button onClick={ganDayNhat}>Gần đây nhất</button>
                      <button onClick={lanXaNhat}>Lần xa nhất</button>
                    </div>
                  </div>
                </div>
              </th>

              <th className="col">Tên Phim</th>
              <th className="col">Số ghế</th>
              <th className="col">Tên Cụm Rạp</th>
              <th className="col">Tên Rạp</th>
            </tr>
          </thead>

          <tbody>{renderTableLsDatve()}</tbody>
        </table>
      </div>
    </div>
  );
}
