import { BrowserRouter, Route, Switch } from "react-router-dom";
import AdminTemplate from "./adminPage";
import AuthAdmin from "./adminPage/AuthPage";
// import LoginHome from "./homePages/LoginHome/LoginHome";
// import PageNotFound from "./homePages/PageNotFound/PageNotFound";
import { routeAdmin, routeHome } from "./route/route";
// import HomeTemplate from "./homePages/HomeTemplate";
import { connect } from "react-redux";
// import * as ActionType from "./homePages/LoginHome/Modules/constants";
import { Component } from "react";
// import SighUp from "./homePages/SighUp/SighUp";
// import Booking from "./homePages/Booking/Booking";
import Login from "./HomePage/login/index";
import HomeTemplete from "./HomePage";
import SignUp from "./HomePage/signUp";
class App extends Component {
  renderRouteHome = (route) => {
    if (route && route.length > 0) {
      return route.map((item, index) => {
        return (
          <HomeTemplete
            key={index}
            exact={item.exact}
            path={item.path}
            component={item.component}
          />
        );
      });
    }
  };

  renderAdminHome = (route) => {
    if (route && route.length > 0) {
      return route.map((item, index) => {
        return (
          <AdminTemplate
            key={index}
            exact={item.exact}
            path={item.path}
            Component={item.component}
          />
        );
      });
    }
  };

  render() {
    return (
      <BrowserRouter>
        <Switch>
          {this.renderAdminHome(routeAdmin)}
          <Route path="/admin/auth" component={AuthAdmin} />
          {/* <Route path="/login" component={LoginHome} />
          <Route path="/sigh-up" component={SighUp} />
          <Route path="/booking/:id" component={Booking} />
          <Route path="*" component={PageNotFound} /> */}

          <Route path="/login" component={Login} />
          <Route path="/sigh-up" component={SignUp} />
          {this.renderRouteHome(routeHome)}
        </Switch>
      </BrowserRouter>
    );
  }
}

export default connect()(App);
