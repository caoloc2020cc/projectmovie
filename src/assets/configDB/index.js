import firebase from "firebase";
const firebaseConfig = {
  apiKey: "AIzaSyBSOIvuRwDEc64moInPxufOqUpsD90A__A",
  authDomain: "facebook-clone-1-50959.firebaseapp.com",
  projectId: "facebook-clone-1-50959",
  storageBucket: "facebook-clone-1-50959.appspot.com",
  messagingSenderId: "854860970206",
  appId: "1:854860970206:web:7af85c666d0ae54a7f2a4f",
  measurementId: "G-PP0LHLMREK",
};
const firebaseApp = firebase.initializeApp(firebaseConfig);
firebase.analytics();
const db = firebaseApp.firestore();
const auth = firebase.auth();
const provider = new firebase.auth.GoogleAuthProvider();
const providerFace = new firebase.auth.FacebookAuthProvider();
export { auth, provider, providerFace };
export default db;
