import React, { Component } from "react";
import "./index.css";

export default class LoaderHome extends Component {
  render() {
    return (
      <div className="loaderHome">
        <div>
          <img
            src="https://tix.vn/app/assets/img/icons/web-logo.png"
            alt="Loading.."
            className="image"
          />
        </div>
      </div>
    );
  }
}
