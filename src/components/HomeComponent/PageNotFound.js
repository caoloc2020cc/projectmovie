import React from "react";

export default function PageNotFound() {
  return (
    <div>
      <img
        src="https://i.stack.imgur.com/vcTyd.png"
        alt="Khong tim thay trang"
        className="w-100 mt-4"
      />
    </div>
  );
}
