import React, { Component } from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as yup from "yup";
import { connect } from "react-redux";
import { NavLink } from "react-router-dom";
import Loader from "../../components/Loader/Loader";
import { fetchSighUpHome } from "./Module/action";
const sighUpUserHomeSchema = yup.object().shape({
  taiKhoan: yup.string().required("* Vui lòng không để trống"),
  matKhau: yup.string().required("* Vui lòng không để trống"),
  email: yup
    .string()
    .required("* Vui lòng không để trống")
    .email("* Vui lòng nhập mail chính xác"),
  soDt: yup
    .string()
    .required("* Vui lòng không để trống")
    .matches(
      /([\+84|84|0]+(3|5|7|8|9|1[2|6|8|9]))+([0-9]{8})\b/,
      "* Số đt ko hợp lệ"
    ),
  hoTen: yup.string().required("* Vui lòng không để trống"),
});
class SignUp extends Component {
  constructor(props) {
    super(props);
  }
  handleSighUp = (values) => {
    this.props.sighUpHome(values, this.props.history);
  };
  renderNoti = () => {
    const { loading, err } = this.props;
    console.log(err);
    return (
      (loading && <Loader />) ||
      (err && <div className="alert alert-danger">{err.response.data}</div>)
    );
  };
  render() {
    return (
      <div className="sighup">
        <div className="container sighup_item">
          <NavLink to="/">
            <img src="./images/logo-test.png" alt="logo" />
          </NavLink>
          <h1>Tạo Tài Khoản Cinema</h1>
          <Formik
            initialValues={{
              taiKhoan: "",
              matKhau: "",
              email: "",
              soDt: "",
              maNhom: "GP09",
              maLoaiNguoiDung: "KhachHang",
              hoTen: "",
            }}
            validationSchema={sighUpUserHomeSchema}
            onSubmit={this.handleSighUp}
            render={(formikProps) => (
              <Form>
                <div className="form-group">
                  <label htmlfor="exampleInputUsername">Tài Khoản</label>
                  <Field
                    type="text"
                    className="form-control"
                    id="exampleInputUsername"
                    name="taiKhoan"
                    onChange={formikProps.handleChange}
                  />
                  <ErrorMessage name="taiKhoan">
                    {(msg) => <div style={{ color: "red" }}>{msg}</div>}
                  </ErrorMessage>
                </div>
                <div className="form-group">
                  <label htmlfor="exampleInputPassword1">Mật Khẩu</label>
                  <Field
                    type="password"
                    className="form-control"
                    id="exampleInputPassword1"
                    name="matKhau"
                    onChange={formikProps.handleChange}
                  />
                  <ErrorMessage name="matKhau">
                    {(msg) => <div style={{ color: "red" }}>{msg}</div>}
                  </ErrorMessage>
                </div>
                <div className="form-group">
                  <label htmlfor="exampleInputEmail">Email</label>
                  <Field
                    type="email"
                    className="form-control"
                    id="exampleInputEmail"
                    name="email"
                    onChange={formikProps.handleChange}
                  />
                  <ErrorMessage name="email">
                    {(msg) => <div style={{ color: "red" }}>{msg}</div>}
                  </ErrorMessage>
                </div>
                <div className="form-group">
                  <label htmlfor="exampleInputNumber">Số Điện Thoại</label>
                  <Field
                    type="text"
                    className="form-control"
                    id="exampleInputNumber"
                    name="soDt"
                    onChange={formikProps.handleChange}
                  />
                  <ErrorMessage name="soDt">
                    {(msg) => <div style={{ color: "red" }}>{msg}</div>}
                  </ErrorMessage>
                </div>
                <div className="form-group">
                  <label htmlfor="exampleInputName">Họ Tên</label>
                  <Field
                    type="text"
                    className="form-control"
                    id="exampleInputName"
                    name="hoTen"
                    onChange={formikProps.handleChange}
                  />
                  <ErrorMessage name="hoTen">
                    {(msg) => <div style={{ color: "red" }}>{msg}</div>}
                  </ErrorMessage>
                </div>
                {this.renderNoti()}
                <button type="submit" className="btn btn-primary">
                  Đăng Ký
                </button>
              </Form>
            )}
          />
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  console.log(state);
  return {
    loading: state.signUpHomeReducer.loading,
    err: state.signUpHomeReducer.err,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    sighUpHome: (user, history) => {
      dispatch(fetchSighUpHome(user, history));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SignUp);
