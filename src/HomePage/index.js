import React from "react";
import Header from "../components/HomeComponent/Header";
import { Route } from "react-router-dom";
function LayoutHome(props) {
  return (
    <>
      <Header props={props} />
      {props.children}
    </>
  );
}
export default function HomeTemplete(props) {
  const { exact, path, component } = props;
  return (
    <LayoutHome>
      <Route exact={exact} path={path} component={component} />
    </LayoutHome>
  );
}
