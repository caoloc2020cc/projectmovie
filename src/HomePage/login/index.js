import React, { useState } from "react";
import { NavLink } from "react-router-dom";
import "./../../assets/sass/Home/login/index.scss";
import { fetchLoginHome, actLoginGoogle } from "./Module/action";
import { connect } from "react-redux";
import Loader from "./../../components/Loader/Loader";
import { auth, provider, providerFace } from "./../../assets/configDB/index";
import "./../../assets/sass/Home/signup/signup.scss";
function Login(props) {
  const [state, setState] = useState({
    taiKhoan: "",
    matKhau: "",
  });

  const handleOnChange = (event) => {
    const { name, value } = event.target;
    setState({
      ...state,
      [name]: value,
    });
  };
  const handleLogin = (event) => {
    event.preventDefault();
    props.fetchLogin(state, props.history);
  };
  const renderNotification = () => {
    const { loading } = props;
    if (loading) {
      return <Loader />;
    }
    const { err } = props;
    if (err) {
      return <div className="alert alert-danger">{err.response.data}</div>;
    }
  };
  /// Sign In with Google
  const signInGoo = () => {
    auth
      .signInWithPopup(provider)
      .then((res) => {
        console.log(res.user);
        // props.fetchLoginGoogle(res.user, props.history);
        localStorage.setItem("userGoo", JSON.stringify(res.user));
        props.history.goBack();
      })
      .catch((err) => alert(err.message));
  };
  //Sign in Face
  const signInFace = () => {
    auth
      .signInWithPopup(providerFace)
      .then((res) => {
        localStorage.setItem("userFace", JSON.stringify(res.user));
        props.history.goBack();
        console.log(res);
      })
      .catch((err) => alert(err.message));
  };
  return (
    <div className="login">
      <div className="container login_item">
        <NavLink to="/">
          <img src="./images/logo-test.png" alt="logo" />
        </NavLink>
        {renderNotification()}
        <form onSubmit={handleLogin}>
          <div className="form-group">
            <label htmlfor="exampleInputUsername">Username</label>
            <input
              type="text"
              className="form-control"
              id="exampleInputUsername"
              name="taiKhoan"
              onChange={handleOnChange}
            />
          </div>
          <div className="form-group mb-1">
            <label htmlfor="exampleInputPassword1">Password</label>
            <input
              type="password"
              className="form-control"
              id="exampleInputPassword1"
              name="matKhau"
              onChange={handleOnChange}
            />
          </div>
          <div className="col-auto">
            <div className="form-group form-check mb-2">
              <input
                className="form-check-input"
                type="checkbox"
                id="autoSizingCheck"
              />
              <label className="form-check-label" htmlfor="autoSizingCheck">
                Remember Me
              </label>
            </div>
          </div>
          <button type="submit" className="btn btn-primary">
            Đăng Nhập
          </button>
          <NavLink
            className="nav-link"
            href="#"
            style={{ display: "inline-block" }}
            to="/sigh-up"
          >
            Đăng Ký
          </NavLink>
        </form>
        <NavLink className="close_login" to="/">
          <i className="fa fa-times" />
        </NavLink>
        <p className="text-center mb-0">--Hoặc--</p>
        <button className="button-face" onClick={signInFace}>
          <img src="https://tix.vn/app/assets/img/login/btn-FB.png" />
        </button>
        <button className="button-google" onClick={signInGoo}>
          <img src="https://tix.vn/app/assets/img/login/btn-Google.png" />
        </button>
      </div>
    </div>
  );
}
const mapStateToProps = (state) => {
  return {
    loading: state.loginHomeReducer.loading,
    err: state.loginHomeReducer.err,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchLogin: (user, history) => {
      dispatch(fetchLoginHome(user, history));
    },
    fetchLoginGoogle: (user, history) => {
      dispatch(actLoginGoogle(user, history));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
