import React, { Component } from "react";
import { fetchDetailMovieHome } from "./module/action";
import { connect } from "react-redux";
import "./../../assets/sass/Home/detail/Comment/detail.scss";
import { makeStyles } from "@material-ui/core/styles";
import CircularProgress from "@material-ui/core/CircularProgress";
import LoaderHome from "./../../components/loaderHome/index";
import Modal from "./../TrangChu/Modal/index";
// import from modal in Trang chu to show a modal to play video
import { setMovie } from "./../TrangChu/Modal/module/action";
//import action trong detail theater de load data xem lich chieu co khong
import { fetchTheaterCLUSTERDETAIL } from "./theaterDetail/module/action";
import TheaterHome from "./theaterDetail";
const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    "& > * + *": {
      marginLeft: theme.spacing(2),
    },
    width: "100%",
    height: "100%",
    position: "relative",
  },
}));

function CircularDeterminate({ danhGia }) {
  const classes = useStyles();
  let danhGiacirle = danhGia * 10;
  return (
    <div className={classes.root}>
      <div className="circleBorder w-100"></div>
      <CircularProgress
        variant="determinate"
        id="circleSuccess"
        className="ml-0"
        thickness={2.8}
        value={danhGiacirle}
      />

      <h2 id="soDiem">{danhGia}</h2>
    </div>
  );
}
class DetailMovie extends Component {
  componentDidMount() {
    const { id } = this.props.match.params;
    this.props.getDetailMovie(id);
    //lay data cho theater detail
    this.props.getListTheater(id);
  }
  //   renderNgayCC = (data) => {
  //     let date = new Date(data.ngayCongChieu);
  //     let day = date.getDate();
  //     return day;
  //   };
  renderStart = (danhGia) => {
    if (danhGia >= 0 && danhGia < 2) {
      return (
        <div className="smallStar">
          <img src="/images/star1.2.png" alt />
        </div>
      );
    } else if (danhGia == 2) {
      return (
        <div className="smallStar">
          <img src="/images/star1.png" alt />
        </div>
      );
    } else if (danhGia > 2 && danhGia < 4) {
      return (
        <>
          <div className="smallStar">
            <img src="/images/star1.png" alt />
          </div>
          <div className="smallStar">
            <img src="/images/star1.2.png" alt />
          </div>
        </>
      );
    } else if (danhGia == 4) {
      return (
        <>
          <div className="smallStar">
            <img src="/images/star1.png" alt />
          </div>
          <div className="smallStar">
            <img src="/images/star1.png" alt />
          </div>
        </>
      );
    } else if (danhGia > 4 && danhGia < 6) {
      return (
        <>
          <div className="smallStar">
            <img src="/images/star1.png" alt />
          </div>
          <div className="smallStar">
            <img src="/images/star1.png" alt />
          </div>
          <div className="smallStar">
            <img src="/images/star1.2.png" alt />
          </div>
        </>
      );
    } else if (danhGia == 6) {
      return (
        <>
          <div className="smallStar">
            <img src="/images/star1.png" alt />
          </div>
          <div className="smallStar">
            <img src="/images/star1.png" alt />
          </div>
          <div className="smallStar">
            <img src="/images/star1.png" alt />
          </div>
        </>
      );
    } else if (danhGia > 6 && danhGia < 8) {
      return (
        <>
          <div className="smallStar">
            <img src="/images/star1.png" alt />
          </div>
          <div className="smallStar">
            <img src="/images/star1.png" alt />
          </div>
          <div className="smallStar">
            <img src="/images/star1.png" alt />
          </div>
          <div className="smallStar">
            <img src="/images/star1.2.png" alt />
          </div>
        </>
      );
    } else if (danhGia == 8) {
      return (
        <>
          <div className="smallStar">
            <img src="/images/star1.png" alt />
          </div>
          <div className="smallStar">
            <img src="/images/star1.png" alt />
          </div>
          <div className="smallStar">
            <img src="/images/star1.png" alt />
          </div>
          <div className="smallStar">
            <img src="/images/star1.png" alt />
          </div>
        </>
      );
    } else if (danhGia > 8 && danhGia < 10) {
      return (
        <>
          <div className="smallStar">
            <img src="/images/star1.png" alt />
          </div>
          <div className="smallStar">
            <img src="/images/star1.png" alt />
          </div>
          <div className="smallStar">
            <img src="/images/star1.png" alt />
          </div>
          <div className="smallStar">
            <img src="/images/star1.png" alt />
          </div>
          <div className="smallStar">
            <img src="/images/star1.png" alt />
          </div>
        </>
      );
    } else if (danhGia == 10) {
      return (
        <>
          <div className="smallStar">
            <img src="/images/star1.png" alt />
          </div>
          <div className="smallStar">
            <img src="/images/star1.png" alt />
          </div>
          <div className="smallStar">
            <img src="/images/star1.png" alt />
          </div>
          <div className="smallStar">
            <img src="/images/star1.png" alt />
          </div>
          <div className="smallStar">
            <img src="/images/star1.png" alt />
          </div>
        </>
      );
    }
  };
  renderInfoMovie = () => {
    const { data } = this.props;
    if (data) {
      return (
        <>
          <div className="movie">
            <div className="movie-img styleBlur">
              <img
                src={data.hinhAnh ? data.hinhAnh : ""}
                alt="background-detail-movie"
              />
            </div>
            <div className="styleGradient" />
            <div className="row filmPosterTop">
              <div
                className="col-3 filmInTop"
                // style={{
                //   backgroundImage: `url(${data.hinhAnh})`,
                // }}
              >
                <div className="posterMain">
                  <img
                    src={data.hinhAnh ? data.hinhAnh : ""}
                    style={{ width: "100%", height: "100%" }}
                  />
                  <button
                    className="playTrailerDetail"
                    type="button"
                    data-toggle="modal"
                    data-target={"#" + data.biDanh}
                    onClick={() => this.props.getModalItem(data)}
                  >
                    <img src="/images/play-video.png" alt />
                  </button>
                </div>
              </div>
              <div className="col-5 infoMain">
                <div>
                  <span className="detailMainInfo1">
                    {/* {this.conversionDate(data.ngayKhoiChieu)} */}
                  </span>
                </div>
                <div>
                  <span>
                    <span className="ageType ">{data.maNhom}</span>
                    <span className="detailMainInfo1 h2">{data.tenPhim}</span>
                  </span>
                </div>
                <div>
                  <span className="detailMainInfo1">
                    {data.lichChieu[0] ? data.lichChieu[0].thoiLuong : "120"}{" "}
                    phút - 0 IMDb - 2D/Digital
                  </span>
                  <br />
                </div>
                <p>Nội dung</p>
                <div className="movie-content">
                  <p>{data.moTa}</p>
                </div>
                <p>
                  Ngày công chiếu:{" "}
                  <span>{` ${new Date(data.ngayKhoiChieu).getDate()}.${new Date(
                    data.ngayKhoiChieu
                  ).getMonth()}.${new Date(
                    data.ngayKhoiChieu
                  ).getFullYear()}`}</span>
                </p>
              </div>
              <div className="col-2"></div>
              <div className="col-2 circleStar">
                <div className="cirlcePercent c100">
                  <CircularDeterminate danhGia={data.danhGia} />
                </div>
                <div className="starMain justify-content-center">
                  {this.renderStart(data.danhGia)}
                </div>
              </div>
            </div>
          </div>
          <div className="banner-mobile d-none">
            <div className="posterMobile">
              <img src={data.hinhAnh} />
              <button
                className="playTrailerMobile"
                type="button"
                data-toggle="modal"
                data-target={"#" + data.biDanh}
                // onClick={() => this.props.getModalItem(data)}
              >
                <img src="/images/play-video.png" alt />
              </button>
              <div className="styleGradient" />
            </div>
            <div className="detail-mobile">
              <div className="col-6 infoMobile">
                <div>
                  <span className="date-time">
                    {/* {this.conversionDate(data.ngayKhoiChieu)} */}
                  </span>
                </div>
                <div>
                  <span>
                    <span className="ageType ">{data.maNhom}</span>
                    <span className="detailMainInfo1 h2">{data.tenPhim}</span>
                  </span>
                </div>
                <div>
                  <span className="date-time">phút - 0 IMDb - 2D/Digital</span>
                  <br />
                </div>
              </div>
              <div className="col-6 circleStar">
                <div className="cirlcePercent c100">
                  {/* <CircularDeterminate danhGia={data.danhGia} /> */}
                </div>
                <div className="starMain justify-content-center">
                  {/* {this.renderStart(data.danhGia)} */}
                </div>
              </div>
            </div>
            <div className="content-mobile">
              <p>Nội dung</p>
              <p>{data.moTa}</p>
            </div>
          </div>
        </>
      );
    }
  };
  render() {
    if (this.props.loading === true) {
      return <LoaderHome />;
    }
    return (
      <>
        <Modal />
        <div className="detail-movie">
          {this.renderInfoMovie()}
          <div className="contentMain">
            <ul className="nav-center mb-3">
              {/* {this.props.data&&this.props.data[0]} */}
              <li className="lich-chieu">
                <button
                  href="#"
                  className="showingInfo h3  text-decoration-none  px-2 mr-2 btn btn-outline-dark text-white "
                  // onClick={this.renderLichChieu}
                >
                  Lịch Chiếu
                </button>
              </li>
              <li>
                <button
                  href="#"
                  className="commentTab h3 text-decoration-none px-2 btn btn-outline-dark text-white"
                  //   onClick={this.renderComment}
                >
                  Đánh Giá
                </button>
              </li>
            </ul>
            <TheaterHome />
          </div>
        </div>
      </>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    loading: state.detailMovieHomeReducer.loading,
    data: state.detailMovieHomeReducer.data,
    dataTheater: state.theaterCLUSTERDETAILReducer.data,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getDetailMovie: (id) => {
      dispatch(fetchDetailMovieHome(id));
    },
    getModalItem: (movie) => {
      dispatch(setMovie(movie));
    },
    getListTheater: (id) => {
      dispatch(fetchTheaterCLUSTERDETAIL(id));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DetailMovie);
