import React, { Component } from "react";
import { pickerDate } from "./module/action";
import { connect } from "react-redux";
class TheaterCluster extends Component {
  namNhuan = (year) => {
    if (
      (year % 4 === 0 && year % 100 !== 0 && year % 400 !== 0) ||
      (year % 100 === 0 && year % 400 === 0)
    )
      return 0;
    else return 1;
  };

  tinhThang = (m, y) => {
    if (this.namNhuan(y) === 0) {
      if (m == 2) {
        return 29;
      } else if (
        m == 1 ||
        m == 3 ||
        m == 5 ||
        m == 7 ||
        m == 8 ||
        m == 10 ||
        m == 12
      ) {
        return 31;
      } else {
        return 30;
      }
    } else {
      if (m == 2) {
        return 28;
      } else if (
        m == 1 ||
        m == 3 ||
        m == 5 ||
        m == 7 ||
        m == 8 ||
        m == 10 ||
        m == 12
      ) {
        return 31;
      } else {
        return 30;
      }
    }
  };

  tinhNgay = () => {
    let d = new Date();
    let date = d.getDate();
    let thu = d.getDay();
    let month = d.getMonth() + 1;
    let year = d.getFullYear();
    let soNgay = this.tinhThang(month, year);
    let dkdate = date;
    console.log(thu);
    let tuan = ["CN", "T2", "T3", "T4", "T5", "T6", "T7"];
    let i = 0;
    let kq = [];

    while (i < 14) {
      let item = { thu: "", ngayThang: "", year, date, month };
      let my = `${date}.${month}`;
      item.ngayThang = my;
      item.thu = tuan[thu];
      kq.push(item);

      if (thu == 6) {
        thu = 0;
      } else {
        thu += 1;
      }
      if (soNgay < dkdate + 14 && date == soNgay) {
        date = 1;
        if (month == 12) {
          month = 1;
          year += 1;
        } else {
          month += 1;
        }
      } else {
        date += 1;
      }
      i++;
    }
    return kq;
  };

  renderNgayThang = () => {
    let kq = this.tinhNgay();
    return kq.map((item, index) => {
      if (index == 0) {
        return (
          <li key={index}>
            <a
              onClick={(e) => {
                if (e.target.className.indexOf("p-active") === -1) {
                  let array = document.getElementsByClassName("pickerDate");
                  console.log(array, item);
                  // array.forEach((item) => {
                  //   item.className = "pickerDate";
                  // });
                  for (let i = 0; i < array.length; i++) {
                    array[i].className = "pickerDate";
                  }
                  e.target.className = "pickerDate p-active";
                }
              }}
            >
              <p className="pickerDate p-active">
                {item.thu}
                <br></br>
                {item.ngayThang}
              </p>
            </a>
          </li>
        );
      } else {
        return (
          <li key={index}>
            <a
              onClick={(e) => {
                if (e.target.className.indexOf("p-active") === -1) {
                  this.props.pickDate(item);
                  let array = document.getElementsByClassName("pickerDate");
                  console.log(array);
                  // array.forEach((item) => {
                  //   item.className = "pickerDate";
                  // });
                  for (let i = 0; i < array.length; i++) {
                    array[i].className = "pickerDate";
                  }
                  e.target.className = "pickerDate p-active";
                }
              }}
            >
              <p className="pickerDate">
                {item.thu}
                <br></br>
                {item.ngayThang}
              </p>
            </a>
          </li>
        );
      }
    });
  };
  render() {
    return <ul className="scroll-bar">{this.renderNgayThang()}</ul>;
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    pickDate: (data) => {
      dispatch(pickerDate(data));
    },
  };
};

export default connect(null, mapDispatchToProps)(TheaterCluster);
