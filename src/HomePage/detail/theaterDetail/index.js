import React, { Component } from "react";
import "./../../../assets/sass/Home/detail/Comment/DetailCumRap.scss";
import { connect } from "react-redux";
import Loader from "../../../components/Loader/Loader";
import TheaterCluster from "./TheaterCluster";
import TheaterShowTime from "./TheaterShowTime";
import { pickerTheater } from "./module/action";
class TheaterHome extends Component {
  autoFocusFirtTheater = () => {
    const { loading, thearterSystem } = this.props;

    console.log(thearterSystem, "index", this.props);
    if (loading) return <Loader />;
    if (thearterSystem.heThongRapChieu.length !== 0) {
      return thearterSystem.heThongRapChieu.map((item, index) => {
        console.log(item);
        if (index === 0) {
          this.props.pickTheater(item);
          return (
            <li key={index}>
              {/* {this.props.getTheaterCluster(item.maHeThongRap)} */}
              <a
                onClick={(e) => {
                  console.log(e.target.className, e.target);
                  if (e.target.className.indexOf("active") === -1) {
                    this.props.pickTheater(item);
                    let arrayLogoName = document.getElementsByClassName("logo");
                    for (let i = 0; i < arrayLogoName.length; i++) {
                      arrayLogoName[i].className = arrayLogoName[
                        i
                      ].className.replace("active", "");
                    }
                    e.target.className += " active";
                  }
                }}
              >
                <img src={item.logo} className="logo active" />
              </a>
            </li>
          );
        } else {
          return (
            <li key={index}>
              <a
                onClick={(e) => {
                  console.log(item);

                  if (e.target.className.indexOf("active") === -1) {
                    // let arrayLogoName = document.getElementsByClassName("logo");
                    // this.props.getTheaterCluster(item.maHeThongRap);
                    this.props.pickTheater(item);
                    let arrayLogoName = document.getElementsByClassName("logo");

                    for (let i = 0; i < arrayLogoName.length; i++) {
                      arrayLogoName[i].className = arrayLogoName[
                        i
                      ].className.replace("active", "");
                    }
                    e.target.className += " active";
                  }
                }}
              >
                <img src={item.logo} className="logo" />
              </a>
            </li>
          );
        }
      });
    }
  };
  render() {
    return (
      <div>
        <section id="cum-rap" className="theaters">
          <div className="container">
            <div className="theaters__info">
              <ul className="theaters__logo">
                {/* {this.renderLogoThearterSystem()} */}
                {this.props.thearterSystem ? (
                  <>{this.autoFocusFirtTheater()}</>
                ) : (
                  <></>
                )}
              </ul>
              <div className="theaters__address">
                <TheaterCluster />
              </div>
              <div className="theaters__film scroll-bar">
                <TheaterShowTime />
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    // movie: state.listMovieHomeReducer.data,
    loading: state.theaterCLUSTERDETAILReducer.loading,
    thearterSystem: state.theaterCLUSTERDETAILReducer.data,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    pickTheater: (data) => {
      dispatch(pickerTheater(data));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(TheaterHome);
