import * as ActionType from "./constants";
import axios from "axios";

export const fetchTheaterCLUSTERDETAIL = (idSystem) => {
  console.log(idSystem);
  return (dispatch) => {
    dispatch(actTheaterCLUSTERDETAILRequest());
    axios({
      url: `https://movie0706.cybersoft.edu.vn/api/QuanLyRap/LayThongTinLichChieuPhim?MaPhim=${idSystem}`,
      method: "GET",
    })
      .then((res) => {
        console.log("data trong  theater Cluster detail", res.data);
        let rapKhong = res.data.heThongRapChieu.filter((item) => {
          console.log(item);

          let phimKhong = item.cumRapChieu.filter((phim) => {
            console.log(phim);

            let data = phim.lichChieuPhim.filter((lich) => {
              return new Date(lich.ngayChieuGioChieu) >= new Date();
            });
            phim.lichChieuPhim = data;
            console.log(data, phim);
            return phim.lichChieuPhim.length !== 0;
          });
          item.cumRapChieu = phimKhong;
          console.log(item);
          return item.cumRapChieu.length !== 0;
        });
        res.data.heThongRapChieu = rapKhong;

        console.log("sau khi filter", res.data);
        dispatch(actTheaterCLUSTERDETAILSuccess(res.data));
      })
      .catch((err) => {
        dispatch(actTheaterCLUSTERDETAILFail(err));
      });
  };
};
export const pickerDate = (data) => {
  return (dispatch) => {
    dispatch(actTheaterPICKERDATEDETAILSuccess(data));
  };
};
export const pickerTheater = (data) => {
  console.log(data);
  return (dispatch) => {
    dispatch(actTheaterPICKERTHEATERDETAILSuccess(data));
  };
};
const actTheaterCLUSTERDETAILRequest = () => {
  return {
    type: ActionType.THEATER_CLUSTER_DETAIL_REQUEST,
  };
};

const actTheaterCLUSTERDETAILSuccess = (data) => {
  return {
    type: ActionType.THEATER_CLUSTER_DETAIL_SUCCESS,
    payload: data,
  };
};

const actTheaterCLUSTERDETAILFail = (err) => {
  return {
    type: ActionType.THEATER_CLUSTER_DETAIL_FAIL,
    payload: err,
  };
};
const actTheaterPICKERDATEDETAILSuccess = (data) => {
  return {
    type: ActionType.THEATER_PICKERDATE_DETAIL_SUCCESS,
    payload: data,
  };
};
const actTheaterPICKERTHEATERDETAILSuccess = (data) => {
  return {
    type: ActionType.THEATER_PICKERTHEATER_DETAIL_SUCCESS,
    payload: data,
  };
};

// export const actTheaterSHOWTIMEDETAILSuccess = (data) => {
//   return {
//     type: ActionType.THEATER_SHOWTIME_DETAIL_SUCCESS,
//     payload: data,
//   };
// };
