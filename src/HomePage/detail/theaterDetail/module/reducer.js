import * as ActionType from "./constants";

let initialState = {
  loading: false,
  data: null,
  err: null,
  ngayThang: `${new Date().getFullYear()}-${
    new Date().getMonth() + 1 >= 1 && new Date().getMonth() + 1 < 10
      ? `0${new Date().getMonth() + 1}`
      : new Date().getMonth() + 1
  }-${
    new Date().getDate() >= 1 && new Date().getDate() < 10
      ? `0${new Date().getDate()}`
      : new Date().getDate()
  }`,
  theater: null,
};

const theaterCLUSTERDETAILReducer = (state = initialState, action) => {
  switch (action.type) {
    case ActionType.THEATER_CLUSTER_DETAIL_REQUEST:
      state.loading = true;
      state.data = null;
      state.err = null;
      return { ...state };
    case ActionType.THEATER_CLUSTER_DETAIL_SUCCESS:
      state.loading = false;
      state.data = action.payload;
      state.err = null;
      return { ...state };
    case ActionType.THEATER_CLUSTER_DETAIL_FAIL:
      state.loading = false;
      state.data = null;
      state.err = action.payload;
      return { ...state };
    case ActionType.THEATER_PICKERDATE_DETAIL_SUCCESS:
      action.payload = `${action.payload.year}-${
        action.payload.month >= 1 && action.payload.month < 10
          ? `0${action.payload.month}`
          : action.payload.month
      }-${
        action.payload.date >= 1 && action.payload.date < 10
          ? `0${action.payload.date}`
          : action.payload.date
      }`;
      console.log(action.payload);
      state.loading = false;
      state.ngayThang = action.payload;
      state.err = null;
      return { ...state };
    case ActionType.THEATER_PICKERTHEATER_DETAIL_SUCCESS:
      console.log(action.payload);
      state.loading = false;
      state.theater = action.payload;
      state.err = null;
      return { ...state };

    default:
      return { ...state };
  }
};

export default theaterCLUSTERDETAILReducer;
