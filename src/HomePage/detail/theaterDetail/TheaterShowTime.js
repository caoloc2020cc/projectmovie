import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

class TheaterShowTime extends Component {
  constructor(props) {
    super(props);
    this.state = {
      imagesCinema: [
        {
          maCumRap: "bhd",
          img: "https://booking.bhdstar.vn/CDN/Image/Entity/CinemaGallery/0000000001?width=1024&height=316",
        },
        {
          maCumRap: "cgv",
          img: "https://www.cgv.vn/media/wysiwyg/about-5.PNG",
        },
        {
          maCumRap: "cns",
          img: "https://list.vn/wp-content/uploads/2020/11/quang-cao-tai-rap-chieu-phim-5-1024x679.jpg",
        },
        {
          maCumRap: "glx",
          img: "https://hkd.com.vn/ckfinder/userfiles/images/DU%20AN/26-2.png",
        },
        {
          maCumRap: "lot",
          img: "https://quangcaovietnam.com.vn/wp-content/uploads/2019/03/quang-cao-tai-rap-chieu-phim-lotte-cinema-3.jpg",
        },
        {
          maCumRap: "meg",
          img: "https://rapchieuphim.com/photos/3/rap-mega-gs-cao-thang.jpg",
        },
      ],
    };
  }
  renderTheaterShowTimes = () => {
    // const { thearterSystem } = this.props;
    console.log(this.props);
    const { theater, ngayThang } = this.props;
    // if (listTheaterMovie) {
    //   if (listTheaterMovie.danhSachPhim.length !== 0) {
    // return listTheaterMovie.danhSachPhim.map((item) => {
    // if(thearterSystem){

    // }
    if (theater) {
      if (ngayThang) {
        return (
          <>
            <div className="film__item">
              <div className="film__info">
                <div className="film__img">
                  <div>
                    <img src="./images/bhd-img-theater.jpg" />
                  </div>
                </div>
                <div
                  className="film__detail"
                  style={{ textDecoration: "none", color: "#000" }}
                >
                  <p className="name" style={{ color: "green" }}>
                    {/* <span>{item.maPhim}</span> */}
                    BHD Start
                  </p>
                  <p className="time">Cinema - IMDB - Steven</p>
                </div>
              </div>
              <div className="film__schedule">
                <div className="film__quality">2D Digital</div>
                <div className="film__time">
                  {/* {item.lstLichChieuTheoPhim.map((items) => {
                              return (
                                <Link
                                  key={items.maLichChieu}
                                  to={`/booking/${items.maLichChieu}`}
                                >
                                  <span>
                                    {new Date(
                                      items.ngayChieuGioChieu
                                    ).toLocaleTimeString()}
                                  </span>
                                </Link>
                              );
                            })} */}
                  <Link
                    //   key={items.maLichChieu}
                    to={`/booking`}
                  >
                    <span>{new Date().toLocaleTimeString()}</span>
                  </Link>
                </div>
              </div>
            </div>
          </>
        );
      } else {
        return <p>Hãy chọn ngày đi!!</p>;
      }
    }

    // });
    // }
    //   } else {
    //     return <p className="text-center mt-5">Không có suất chiếu</p>;
    //   }
    // }
  };
  render() {
    return <>{this.renderTheaterShowTimes()}</>;
  }
}
const mapStateToProps = (state) => {
  return {
    // movie: state.listMovieHomeReducer.data,
    loading: state.theaterCLUSTERDETAILReducer.loading,
    theater: state.theaterCLUSTERDETAILReducer.theater,
    ngayThang: state.theaterCLUSTERDETAILReducer.ngayThang,
  };
};

export default connect(mapStateToProps, null)(TheaterShowTime);
