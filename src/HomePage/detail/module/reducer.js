import * as ActionType from "./constants";

let initialState = {
  loading: false,
  data: null,
  err: null,
};

const detailMovieHomeReducer = (state = initialState, action) => {
  switch (action.type) {
    case ActionType.DETAIL_MOVIE_HOME_REQUEST:
      state.loading = true;
      state.data = null;
      state.err = null;
      return { ...state };
    case ActionType.DETAIL_MOVIE_HOME_SUCCESS:
      state.loading = false;
      state.data = action.payload;
      state.err = null;
      return { ...state };
    case ActionType.DETAIL_MOVIE_HOME_FAIL:
      state.loading = false;
      state.data = null;
      state.err = action.payload;
      return { ...state };
    default:
      return { ...state };
  }
};

export default detailMovieHomeReducer;
