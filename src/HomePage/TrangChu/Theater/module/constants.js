export const THEATER_INFO_HOME_REQUEST =
  "@theaterInfoHomeReducer/THEATER_INFO_HOME_REQUEST";
export const THEATER_INFO_HOME_SUCCESS =
  "@theaterInfoHomeReducer/THEATER_INFO_HOME_SUCCESS";
export const THEATER_INFO_HOME_FAIL =
  "@theaterInfoHomeReducer/THEATER_INFO_HOME_FAIL";
