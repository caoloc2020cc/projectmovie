import React, { Component } from "react";
import { Element } from "react-scroll";
import { fetchTheaterInfoHome } from "./module/action";
import { connect } from "react-redux";
import Loader from "../../../components/Loader/Loader";
import TheaterCluster from "./TheaterCluster/index";
import "./../../../assets/sass/Home/theater/index.scss";
// import action from theaterCluster/module
import { fetchTheaterCLUSTERHome } from "./TheaterCluster/module/action";
import TheaterShowTimes from "./TheaterCluster/TheaterShowTimes";
class Theater extends Component {
  // constructor(props){
  //     super(props)

  // }
  componentDidMount() {
    this.props.getTheater();
  }

  autoFocusFirtTheater = () => {
    const { loading, thearterSystem } = this.props;
    if (loading) return <Loader />;
    if (thearterSystem) {
      return thearterSystem.map((item, index) => {
        console.log(item);
        if (index === 0) {
          return (
            <li key={index}>
              {this.props.getTheaterCluster(item.maHeThongRap)}
              <a
                onClick={(e) => {
                  if (e.target.className.indexOf("active") === -1) {
                    this.props.getTheaterCluster(item.maHeThongRap);
                    let arrayLogoName = document.getElementsByClassName("logo");
                    for (let i = 0; i < arrayLogoName.length; i++) {
                      arrayLogoName[i].className = arrayLogoName[
                        i
                      ].className.replace("active", "");
                    }
                    e.target.className += " active";
                  }
                }}
              >
                <img src={item.logo} className="logo active" />
              </a>
            </li>
          );
        } else {
          return (
            <li key={index}>
              <a
                onClick={(e) => {
                  console.log(item);

                  if (e.target.className.indexOf("active") === -1) {
                    // let arrayLogoName = document.getElementsByClassName("logo");
                    this.props.getTheaterCluster(item.maHeThongRap);
                    let arrayLogoName = document.getElementsByClassName("logo");

                    for (let i = 0; i < arrayLogoName.length; i++) {
                      arrayLogoName[i].className = arrayLogoName[
                        i
                      ].className.replace("active", "");
                    }
                    e.target.className += " active";
                  }
                }}
              >
                <img src={item.logo} className="logo" />
              </a>
            </li>
          );
        }
      });
    }
  };
  render() {
    return (
      <div>
        <Element id="cum-rap" className="theaters">
          <div className="container">
            <div className="theaters__info">
              <ul className="theaters__logo">
                {/* {this.renderLogoThearterSystem()} */}
                {this.props.thearterSystem ? (
                  <>{this.autoFocusFirtTheater()}</>
                ) : (
                  <></>
                )}
              </ul>
              <div className="theaters__address scroll-bar">
                <TheaterCluster />
              </div>
              <div className="theaters__film scroll-bar">
                <TheaterShowTimes />
              </div>
            </div>
          </div>
        </Element>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    // movie: state.listMovieHomeReducer.data,
    loading: state.theaterInfoHomeReducer.loading,
    thearterSystem: state.theaterInfoHomeReducer.data,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    //   getTheaterMovie: (maPhim) => {
    //     dispatch(fetchTheaterToolsHome(maPhim));
    //   },
    getTheater: () => {
      dispatch(fetchTheaterInfoHome());
    },
    getTheaterCluster: (maHTRap) => {
      dispatch(fetchTheaterCLUSTERHome(maHTRap));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Theater);
