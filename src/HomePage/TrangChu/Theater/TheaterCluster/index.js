import React, { Component } from "react";
import { connect } from "react-redux";
import { actTheaterSHOWTIMEHomeSuccess } from "./module/action";
class TheaterCluster extends Component {
  constructor(props) {
    super(props);
    this.state = {
      imagesCinema: [
        {
          maCumRap: "bhd",
          img: "./images/bhd-img-theater.jpg",
        },
        {
          maCumRap: "cgv",
          img: "./images/cgv-img-theater.png",
        },
        {
          maCumRap: "cns",
          img: "./images/cns-img-theater.jpg",
        },
        {
          maCumRap: "glx",
          img: "./images/galaxy-img-theater.jpg",
        },
        {
          maCumRap: "lot",
          img: "./images/lotte-img-theater.jpg",
        },
        {
          maCumRap: "meg",
          img: "./images/megastar-img-theater.jpg",
        },
      ],
    };
  }
  renderInfoCluster = () => {
    const { TheaterCluster } = this.props;
    console.log(TheaterCluster);
    if (TheaterCluster) {
      return TheaterCluster[0].lstCumRap.map((item, index) => {
        let imgCinema = this.state.imagesCinema.filter(
          (items) => items.maCumRap === item.maCumRap.slice(0, 3)
        );
        if (index === 0) {
          //auto render choosen 1 movie cluster
          this.props.getListTimeTheaterMovie(item);
          return (
            <div
              key={item.maCupRap}
              className="address__item"
              onClick={(e) => {
                if (e.target.className.indexOf("active") === -1) {
                  this.props.getListTimeTheaterMovie(item);
                  let object = document.getElementsByClassName("shadow__item");
                  for (let i = 0; i < object.length; i++) {
                    object[i].className = object[i].className.replace(
                      "active",
                      ""
                    );
                  }
                  e.target.className += " active";
                }
              }}
            >
              <div className="address__img">
                <img src={imgCinema[0].img} />
              </div>
              <div className="address__info">
                <span style={{ color: "#8bc541" }}>{item.tenCumRap}</span>
                <p>{item.diaChi}</p>
              </div>
              <div className="shadow__item active"></div>
            </div>
          );
        } else {
          return (
            <div
              key={item.maCupRap}
              className="address__item"
              onClick={(e) => {
                this.props.getListTimeTheaterMovie(item);
                if (e.target.className.indexOf("active") === -1) {
                  let object = document.getElementsByClassName("shadow__item");
                  for (let i = 0; i < object.length; i++) {
                    object[i].className = object[i].className.replace(
                      "active",
                      ""
                    );
                  }
                  e.target.className += " active";
                }
              }}
            >
              <div className="address__img">
                <img src={imgCinema[0].img} />
              </div>
              <div className="address__info">
                <span style={{ color: "#8bc541" }}>{item.tenCumRap}</span>
                <p>{item.diaChi}</p>
              </div>
              <div className="shadow__item"></div>
            </div>
          );
        }
      });
    }
  };
  render() {
    return <>{this.renderInfoCluster()}</>;
  }
}
const mapStateToProps = (state) => {
  return {
    TheaterCluster: state.theaterCLUSTERHomeReducer.data,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getListTimeTheaterMovie: (listTheaterMovie) => {
      dispatch(actTheaterSHOWTIMEHomeSuccess(listTheaterMovie));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(TheaterCluster);
