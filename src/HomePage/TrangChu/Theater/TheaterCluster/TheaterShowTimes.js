import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

class TheaterShowTimes extends Component {
  renderTheaterShowTimes = () => {
    const { listTheaterMovie } = this.props;

    if (listTheaterMovie) {
      if (listTheaterMovie.danhSachPhim.length !== 0) {
        return listTheaterMovie.danhSachPhim.map((item) => {
          return (
            <>
              <div key={item.maPhim} className="film__item">
                <div className="film__info">
                  <div className="film__img">
                    <Link to={`/detail-movie/${item.maPhim}`}>
                      <img src={item.hinhAnh} alt />
                    </Link>
                  </div>
                  <Link
                    to={`/detail-movie/${item.maPhim}`}
                    className="film__detail"
                    style={{ textDecoration: "none", color: "#000" }}
                  >
                    <p className="name">
                      <span>{item.maPhim}</span>
                      {item.tenPhim}
                    </p>
                    <p className="time">Cinema - IMDB - Steven</p>
                  </Link>
                </div>
                <div className="film__schedule">
                  <div className="film__quality">2D Digital</div>
                  <div className="film__time">
                    {item.lstLichChieuTheoPhim.map((items) => {
                      return (
                        <Link
                          key={items.maLichChieu}
                          to={`/booking/${items.maLichChieu}`}
                        >
                          <span>
                            {new Date(
                              items.ngayChieuGioChieu
                            ).toLocaleTimeString()}
                          </span>
                        </Link>
                      );
                    })}
                  </div>
                </div>
              </div>
            </>
          );
        });
      } else {
        return <p className="text-center mt-5">Không có suất chiếu</p>;
      }
    }
  };

  render() {
    return <>{this.renderTheaterShowTimes()}</>;
  }
}

const mapStateToProps = (state) => {
  return {
    listTheaterMovie: state.theaterCLUSTERHomeReducer.listTheaterMovie,
  };
};

export default connect(mapStateToProps, null)(TheaterShowTimes);
