import * as ActionType from "./constants";
import axios from "axios";

export const fetchTheaterCLUSTERHome = (idSystem) => {
  console.log(idSystem);
  return (dispatch) => {
    dispatch(actTheaterCLUSTERHomeRequest());
    axios({
      url: `https://movie0706.cybersoft.edu.vn/api/QuanLyRap/LayThongTinLichChieuHeThongRap?maHeThongRap=${idSystem}&maNhom=GP09`,
      method: "GET",
    })
      .then((res) => {
        console.log("data trong action theater Cluster", res.data);
        res.data[0].lstCumRap.forEach((item) => {
          console.log(item);

          let phimKhong = item.danhSachPhim.filter((phim) => {
            console.log(phim);

            let data = phim.lstLichChieuTheoPhim.filter((lich) => {
              return new Date(lich.ngayChieuGioChieu) >= new Date();
            });
            phim.lstLichChieuTheoPhim = data;
            console.log(data, phim);
            return phim.lstLichChieuTheoPhim.length !== 0;
          });
          item.danhSachPhim = phimKhong;
          console.log(item);
        });
        // res.data[0].lstCumRap = rapKhong;
        console.log("sau khi filter", res.data);
        dispatch(actTheaterCLUSTERHomeSuccess(res.data));
      })
      .catch((err) => {
        dispatch(actTheaterCLUSTERHomeFail(err));
      });
  };
};

const actTheaterCLUSTERHomeRequest = () => {
  return {
    type: ActionType.THEATER_CLUSTER_HOME_REQUEST,
  };
};

const actTheaterCLUSTERHomeSuccess = (data) => {
  return {
    type: ActionType.THEATER_CLUSTER_HOME_SUCCESS,
    payload: data,
  };
};

const actTheaterCLUSTERHomeFail = (err) => {
  return {
    type: ActionType.THEATER_CLUSTER_HOME_FAIL,
    payload: err,
  };
};
export const actTheaterSHOWTIMEHomeSuccess = (data) => {
  return {
    type: ActionType.THEATER_SHOWTIME_HOME_SUCCESS,
    payload: data,
  };
};
