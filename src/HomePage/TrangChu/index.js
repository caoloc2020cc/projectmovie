import React, { Component } from "react";
import ListMovie from "./ListMovie";
import Modal from "./Modal";
import Slides from "./Slides";
import Theater from "./Theater";

export default class TrangChu extends Component {
  render() {
    return (
      <>
        <Modal />
        <Slides />
        <ListMovie />
        <Theater />
      </>
    );
  }
}
