import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { setMovie } from "./../../Modal/module/action";
class MovieItem extends Component {
  render() {
    const { movie } = this.props;
    return (
      <div className="film__col">
        <div className="film__img">
          <Link to={`/detail-movie/${movie.maPhim}`}>
            <img src={movie.hinhAnh} alt />
            <div className="film__rate">
              <p>{movie.danhGia}</p>
              <p className="rate__img">
                <img src="./images/star1.png" alt />
                <img src="./images/star1.png" alt />
                <img src="./images/star1.png" alt />
                <img src="./images/star1.png" alt />
                <img src="./images/star1.2.png" alt />
              </p>
            </div>
            <div className="hover__show"></div>
          </Link>
          <button
            type="button"
            className="play__video"
            data-toggle="modal"
            data-target={"#" + movie.biDanh}
            onClick={() => this.props.getMovie(movie)}
          >
            <img src="./images/play-video.png" alt />
          </button>
        </div>
        <div className="film__info">
          <div className="film__name">
            <p>
              <span>{movie.maPhim}</span>
              {movie.tenPhim}
            </p>
          </div>
          <div className="film__time">
            <p>{movie.maNhom}</p>
          </div>
          <Link className="hover__show2" to={`/detail-movie/${movie.maPhim}`}>
            MUA VÉ
          </Link>
        </div>
      </div>
    );
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    getMovie: (modal) => {
      dispatch(setMovie(modal));
    },
  };
};
export default connect(null, mapDispatchToProps)(MovieItem);
