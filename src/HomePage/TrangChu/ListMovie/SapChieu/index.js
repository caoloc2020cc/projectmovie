import React, { Component } from "react";
import { fetchSapChieu } from "./module/action";
import { connect } from "react-redux";

import Loader from "../../../../components/Loader/Loader";
import MovieItem from "../MovieItem";
import "./../../../../assets/sass/Home/lichChieu/dangChieu.scss";
class SapChieu extends Component {
  componentDidMount() {
    this.props.fetchSapChieuIndex();
  }
  renderListMovie1 = () => {
    const { data, loading } = this.props;
    if (loading) {
      return <Loader />;
    } else {
      return data.map((movie) => {
        return <MovieItem key={movie.maPhim} movie={movie} />;
      });
    }

    // for (let i=8,i<data.)
  };
  renderListMovie1_2 = () => {
    // khi data.length lon hon 8 thi active auto tu 1 toi 8
    const { data, loading } = this.props;
    if (loading) {
      return <Loader />;
    } else {
      return data.map((movie, index) => {
        if (index < 8) {
          return <MovieItem key={movie.maPhim} movie={movie} />;
        }
      });
    }

    // for (let i=8,i<data.)
  };
  renderListMovie2_2 = (i, length) => {
    const { data } = this.props;
    let last = length - i;
    if (last <= 8) {
      let dataReduce = data.slice(i, length);
      return dataReduce.map((movie) => {
        return <MovieItem key={movie.maPhim} movie={movie} />;
      });
    } else {
      let dataReduce = data.slice(i, i + 8);
      return dataReduce.map((movie) => {
        return <MovieItem key={movie.maPhim} movie={movie} />;
      });
    }
  };
  renderListMovie2 = () => {
    //xet data.length chac chan lon hon 8 va lon bao nhieu
    const { data } = this.props;
    for (let i = 8; i <= data.length; i += 8) {
      return (
        <div className="carousel-item">
          <div className="show__film">
            {this.renderListMovie2_2(i, data.length)}
          </div>
        </div>
      );
    }
  };
  render() {
    const { data } = this.props;
    return (
      <div
        id="indicators"
        className="carousel slide slides__film"
        data-ride="carousel"
      >
        <div className="carousel-inner">
          {/* <div className="carousel-item active">
            <div className="show__film"></div>
          </div>
          <div className="carousel-item">
            <div className="show__film"></div>
          </div> */}
          {data && data.length <= 8 ? (
            <div className="carousel-item active">
              <div className="show__film">{this.renderListMovie1()}</div>
            </div>
          ) : (
            ""
          )}
          {data && data.length > 8 ? (
            <>
              {" "}
              <div className="carousel-item active">
                <div className="show__film">{this.renderListMovie1_2()}</div>
              </div>
              {this.renderListMovie2()}
            </>
          ) : (
            ""
          )}
        </div>
        <a
          className="carousel-control-prev arrow1__slides"
          href="#indicators"
          role="button"
          data-slide="prev"
        >
          <img className="arrow__img" src="./images/back-session.png" alt />
          <span className="sr-only">Previous</span>
        </a>
        <a
          className="carousel-control-next arrow2__slides"
          href="#indicators"
          role="button"
          data-slide="next"
        >
          <img className="arrow__img" src="./images/next-session.png" alt />
          <span className="sr-only">Next</span>
        </a>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    loading: state.listSapChieu.loading,
    data: state.listSapChieu.data,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchSapChieuIndex: () => {
      dispatch(fetchSapChieu());
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SapChieu);
