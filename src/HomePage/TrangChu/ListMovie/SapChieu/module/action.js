import * as ActionType from "./constants";
import axios from "axios";

export const fetchSapChieu = () => {
  return (dispatch) => {
    dispatch(actSapChieuRequest());
    let d = new Date();
    let date = d.getDate() + 1;
    let month = d.getMonth() + 1;
    let year = d.getFullYear();
    let monthNext = month;
    let yearNext = year;
    if (monthNext >= 1 && monthNext <= 6) {
      monthNext += 6;
    } else if (monthNext > 6 && monthNext <= 12) {
      monthNext = monthNext - 6;
      yearNext += 1;
    }

    if (month >= 1 && month < 10) {
      month = "0" + month;
    }
    if (monthNext >= 1 && monthNext < 10) {
      monthNext = "0" + monthNext;
    }
    console.log(date);
    if (date >= 1 && date < 10) {
      date = "0" + date;
    }

    axios({
      url: `https://movie0706.cybersoft.edu.vn/api/QuanLyPhim/LayDanhSachPhimTheoNgay?maNhom=GP09&soTrang=1&soPhanTuTrenTrang=100&tuNgay=${date}%2F${month}%2F${year}&denNgay=${date}%2F${monthNext}%2F${yearNext}`,
      method: "GET",
    })
      .then((res) => {
        console.log(res.data);
        // let data = res.data.sort(
        //   (a, b) => new Date(b.ngayKhoiChieu) - new Date(a.ngayKhoiChieu)
        // );
        // let listMovie = res.data.sort((nextItem, item) => {
        //   let nextItemMovie = nextItem.ngayKhoiChieu.toLowerCase();
        //   let itemMovie = item.ngayKhoiChieu.toLocaleLowerCase();
        //   if (nextItemMovie > itemMovie) {
        //     return -1;
        //   }
        //   if (nextItemMovie < itemMovie) {
        //     return 1;
        //   }
        //   return 1;
        // });
        dispatch(actSapChieuSuccess(res.data));
      })
      .catch((err) => {
        dispatch(actSapChieuFail(err));
      });
  };
};
const actSapChieuRequest = () => {
  return {
    type: ActionType.LIST_MOVIE_HOME_REQUEST,
  };
};

const actSapChieuSuccess = (data) => {
  return {
    type: ActionType.LIST_MOVIE_HOME_SUCCESS,
    payload: data,
  };
};

const actSapChieuFail = (err) => {
  return {
    type: ActionType.LIST_MOVIE_HOME_FAIL,
    payload: err,
  };
};
