import React, { Component } from "react";
import { Element } from "react-scroll";
import DangChieu from "./DangChieu";
import SapChieu from "./SapChieu";

export default class ListMovie extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isWatch: true,
    };
  }
  handleDangChieu = () => {
    this.setState({
      isWatch: true,
    });
  };
  handleSapChieu = () => {
    this.setState({
      isWatch: false,
    });
  };
  render() {
    return (
      <Element id="lich-chieu" className="showTimes">
        <div className="container">
          {this.state.isWatch ? (
            <>
              <ul className="nav__film">
                <li className="dangChieu styleActive">
                  <a>Đang Chiếu</a>
                </li>
                <li className="sapChieu" style={{ cursor: "pointer" }}>
                  <a onClick={this.handleSapChieu}>Sắp Chiếu</a>
                </li>
              </ul>
              <DangChieu />
            </>
          ) : (
            <>
              <ul className="nav__film">
                <li className="dangChieu" style={{ cursor: "pointer" }}>
                  <a onClick={this.handleDangChieu}>Đang Chiếu</a>
                </li>
                <li className="sapChieu styleActive">
                  <a>Sắp Chiếu</a>
                </li>
              </ul>
              <SapChieu />
            </>
          )}
        </div>
      </Element>
    );
  }
}
