import * as ActionType from "./constants";

let initialState = {
  loading: false,
  data: null,
  err: null,
};

const listMovieHomeReducer = (state = initialState, action) => {
  switch (action.type) {
    case ActionType.LIST_MOVIE_SLIDE_REQUEST:
      state.loading = true;
      state.data = null;
      state.err = null;
      return { ...state };
    case ActionType.LIST_MOVIE_SLIDE_SUCCESS:
      state.loading = false;
      state.data = action.payload;
      state.err = null;
      return { ...state };
    case ActionType.LIST_MOVIE_SLIDE_FAIL:
      state.loading = false;
      state.data = null;
      state.err = action.payload;
      return { ...state };
    default:
      return { ...state };
  }
};

export default listMovieHomeReducer;
