export const LIST_MOVIE_SLIDE_REQUEST =
  "@listMovieHomeReducer/LIST_MOVIE_SLIDE_REQUEST";
export const LIST_MOVIE_SLIDE_SUCCESS =
  "@listMovieHomeReducer/LIST_MOVIE_SLIDE_SUCCESS";
export const LIST_MOVIE_SLIDE_FAIL =
  "@listMovieHomeReducer/LIST_MOVIE_SLIDE_FAIL";
