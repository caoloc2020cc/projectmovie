import * as ActionType from "./constants";
import axios from "axios";

export const fetchListMovieHome = () => {
  return (dispatch) => {
    dispatch(actListMovieSLIDERequest());
    let d = new Date();
    let date = d.getDate();
    let month = d.getMonth() + 1;
    let year = d.getFullYear();
    console.log(date, month, year);
    if (month >= 1 && month < 10) {
      month = "0" + month;
    }
    axios({
      url: `https://movie0706.cybersoft.edu.vn/api/QuanLyPhim/LayDanhSachPhimTheoNgay?maNhom=GP09&soTrang=1&soPhanTuTrenTrang=100&tuNgay=30%2F06%2F2021&denNgay=${date}%2F${month}%2F${year}`,
      method: "GET",
    })
      .then((res) => {
        console.log(res.data);
        // let data = res.data.sort(
        //   (a, b) => new Date(b.ngayKhoiChieu) - new Date(a.ngayKhoiChieu)
        // );
        // let listMovie = res.data.sort((nextItem, item) => {
        //   let nextItemMovie = nextItem.ngayKhoiChieu.toLowerCase();
        //   let itemMovie = item.ngayKhoiChieu.toLocaleLowerCase();
        //   if (nextItemMovie > itemMovie) {
        //     return -1;
        //   }
        //   if (nextItemMovie < itemMovie) {
        //     return 1;
        //   }
        //   return 1;
        // });
        dispatch(actListMovieSLIDESuccess(res.data));
      })
      .catch((err) => {
        dispatch(actListMovieSLIDEFail(err));
      });
  };
};
const actListMovieSLIDERequest = () => {
  return {
    type: ActionType.LIST_MOVIE_SLIDE_REQUEST,
  };
};

const actListMovieSLIDESuccess = (data) => {
  return {
    type: ActionType.LIST_MOVIE_SLIDE_SUCCESS,
    payload: data,
  };
};

const actListMovieSLIDEFail = (err) => {
  return {
    type: ActionType.LIST_MOVIE_SLIDE_FAIL,
    payload: err,
  };
};
