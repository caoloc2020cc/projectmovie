import React, { Component } from "react";
import { fetchTheaterToolsHome } from "./module/action";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
class FindFilm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tenMovie: null,
      rapMovie: null,
      ngayMovie: null,
      lichChieuPhim: null,
      suatChieu: null,
      idLich: null,
    };
  }

  renderMovieTools = () => {
    const { dataMovie } = this.props;
    // if (loading) return <Loader />;
    if (dataMovie) {
      console.log(dataMovie);
      return dataMovie.map((movie) => {
        return (
          <>
            <a
              key={movie.maPhim}
              className="dropdown-item"
              onClick={() => {
                this.handlechosenMovie(movie.tenPhim);
                console.log(this.props);
                this.props.getShowTimesTools(movie.maPhim);
              }}
            >
              {movie.tenPhim}
            </a>
          </>
        );
      });
    }
  };
  handlechosenMovie = (tenPhim) => {
    this.setState({
      tenMovie: tenPhim,
      rapMovie: null,
      ngayMovie: null,
      suatChieu: null,
      idLich: null,
    });
  };
  handleRap = (ten, lich) => {
    this.setState({
      rapMovie: ten,
      ngayMovie: null,
      lichChieuPhim: lich,
      suatChieu: null,
      idLich: null,
    });
  };
  renderTheaterMovie = () => {
    // const { data } = this.props;
    // if (!data) return <p>Hãy chọn Phim</p>;
    // if (loading) return <Loader />;
    // console.log(data);
    // if (data && data.heThongRapChieu[0] !== undefined) {
    //   return data.heThongRapChieu.map((item) =>
    //     item.cumRapChieu.map((rap) => (
    //       <a key={rap.maCumRap} className="dropdown-item">
    //         {rap.tenCumRap}
    //       </a>
    //     ))
    //   );
    // }
    // return <p>Hiện chưa có lịch chiếu phim này...</p>;
    if (this.state.tenMovie) {
      const { data } = this.props;
      if (data && data.heThongRapChieu.length !== 0) {
        return data.heThongRapChieu.map((item) =>
          item.cumRapChieu.map((rap) => (
            <a
              key={rap.maCumRap}
              className="dropdown-item"
              onClick={() => this.handleRap(rap.tenCumRap, rap.lichChieuPhim)}
            >
              {rap.tenCumRap}
            </a>
          ))
        );
      }
    } else {
      return <p>Hãy chọn phim</p>;
    }
  };
  handleDate = (date) => {
    this.setState({
      ngayMovie: date,
      suatChieu: null,
      idLich: null,
    });
  };
  renderDateTools = () => {
    const { tenMovie, rapMovie, lichChieuPhim } = this.state;
    if (tenMovie) {
      if (rapMovie) {
        if (lichChieuPhim.length !== 0) {
          let reduceLich = [];
          lichChieuPhim.map((item) => {
            if (
              !reduceLich.includes(
                new Date(item.ngayChieuGioChieu).toISOString().slice(0, 10)
              )
            ) {
              reduceLich.push(
                new Date(item.ngayChieuGioChieu).toISOString().slice(0, 10)
              );
            }
          });
          return reduceLich.map((item, index) => {
            return (
              <a
                key={index}
                className="dropdown-item"
                onClick={() => this.handleDate(item)}
              >
                {item}
              </a>
            );
          });
        }
      } else {
        return <p>Hãy chọn Rạp</p>;
      }
    } else {
      return <p>Hãy chọn Phim và Rạp</p>;
    }
  };
  handleTime = (idLich, xuat) => {
    this.setState({
      suatChieu: xuat,
      idLich,
    });
  };
  renderTimesTools = () => {
    const { tenMovie, rapMovie, ngayMovie, lichChieuPhim } = this.state;
    if (tenMovie) {
      if (rapMovie) {
        if (ngayMovie) {
          if (lichChieuPhim.length !== 0) {
            return lichChieuPhim.map((item) => {
              if (
                new Date(item.ngayChieuGioChieu).toISOString().slice(0, 10) ==
                this.state.ngayMovie
              ) {
                return (
                  <a
                    key={item.maLichChieu}
                    className="dropdown-item"
                    onClick={() =>
                      this.handleTime(
                        item.maLichChieu,
                        `${new Date(
                          item.ngayChieuGioChieu
                        ).toLocaleTimeString()}-${item.tenRap}`
                      )
                    }
                  >
                    {`${new Date(
                      item.ngayChieuGioChieu
                    ).toLocaleTimeString()}-${item.tenRap}`}
                  </a>
                );
              }
            });
          }
        } else {
          return <p>Hãy chọn Ngày Xem</p>;
        }
      } else {
        return <p>Hãy chọn Rạp và Ngày Xem</p>;
      }
    } else {
      return <p>Hãy chọn Phim, Rạp và Ngày Xem</p>;
    }
  };
  getBooking = () => {
    console.log(this.state);
  };
  render() {
    return (
      <div className="select__menu">
        <div className="select__film">
          <div
            className="btn"
            id="Phim"
            data-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false"
          >
            <p>{this.state.tenMovie ? this.state.tenMovie : "Phim"}</p>
          </div>
          <div
            className="dropdown-menu"
            style={{ width: "28%" }}
            aria-labelledby="Phim"
          >
            {this.renderMovieTools()}
          </div>
        </div>
        <div className="btn-group">
          <div className="dropdown detail__menu rap">
            <div
              className="btn"
              id="Rap"
              data-toggle="dropdown"
              aria-haspopup="true"
              aria-expanded="false"
            >
              <p>{this.state.rapMovie ? this.state.rapMovie : "Rạp"}</p>
            </div>
            <div
              className="dropdown-menu"
              aria-labelledby="Rap"
              onClick={this.handleChosenTheater}
            >
              {this.renderTheaterMovie()}
            </div>
          </div>
          {/* <DateTools /> */}
          <div className="dropdown detail__menu">
            <div
              className="btn"
              id="ngayXem"
              data-toggle="dropdown"
              aria-haspopup="true"
              aria-expanded="false"
            >
              <p>{this.state.ngayMovie ? this.state.ngayMovie : "Ngày Xem"}</p>
            </div>
            <div
              className="dropdown-menu"
              aria-labelledby="ngayXem"
              onClick={this.handleChosenDate}
            >
              {this.renderDateTools()}
            </div>
          </div>
          <div className="dropdown detail__menu">
            <div
              className="btn"
              id="suatChieu"
              data-toggle="dropdown"
              aria-haspopup="true"
              aria-expanded="false"
            >
              <p>
                {this.state.suatChieu ? this.state.suatChieu : "Xuất Chiếu"}
              </p>
            </div>
            <div
              className="dropdown-menu"
              aria-labelledby="suatChieu"
              onClick={this.handleChosenDate}
            >
              {this.renderTimesTools()}
            </div>
          </div>
          <div className="book__tickets detail__menu">
            {this.state.suatChieu ? (
              <Link to={`/booking/${this.state.idLich}`}>MUA VÉ NGAY</Link>
            ) : (
              <button>MUA VÉ NGAY</button>
            )}
          </div>
        </div>
        {/* <TheaterTools /> */}
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    loading: state.theaterToolsHomeReducer.loading,
    data: state.theaterToolsHomeReducer.data,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getShowTimesTools: (maPhim) => {
      dispatch(fetchTheaterToolsHome(maPhim));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(FindFilm);
