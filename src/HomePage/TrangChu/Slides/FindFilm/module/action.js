import * as ActionType from "./constants";
import axios from "axios";

export const fetchTheaterToolsHome = (id) => {
  return (dispatch) => {
    dispatch(actTheaterToolsHomeRequest());
    axios({
      url: `https://movie0706.cybersoft.edu.vn/api/QuanLyRap/LayThongTinLichChieuPhim?MaPhim=${id}`,
      method: "GET",
    })
      .then((res) => {
        let rapKhong = res.data.heThongRapChieu.filter((item) => {
          let phimKhong = item.cumRapChieu.filter((phim) => {
            let data = phim.lichChieuPhim.filter((lich) => {
              return new Date(lich.ngayChieuGioChieu) >= new Date();
            });
            phim.lichChieuPhim = data;
            return phim.lichChieuPhim.length !== 0;
          });
          item.cumRapChieu = phimKhong;
          return item.cumRapChieu.length !== 0;
        });
        res.data.heThongRapChieu = rapKhong;

        dispatch(actTheaterToolsHomeSuccess(res.data));
      })
      .catch((err) => {
        dispatch(actTheaterToolsHomeFail(err));
      });
  };
};

const actTheaterToolsHomeRequest = () => {
  return {
    type: ActionType.THEATER_TOOLS_HOME_REQUEST,
  };
};

const actTheaterToolsHomeSuccess = (data) => {
  return {
    type: ActionType.THEATER_TOOLS_HOME_SUCCESS,
    payload: data,
  };
};

const actTheaterToolsHomeFail = (err) => {
  return {
    type: ActionType.THEATER_TOOLS_HOME_FAIL,
    payload: err,
  };
};
