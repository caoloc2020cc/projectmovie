export const THEATER_TOOLS_HOME_REQUEST =
  "@theaterToolsHomeReducer/THEATER_TOOLS_HOME_REQUEST";
export const THEATER_TOOLS_HOME_SUCCESS =
  "@theaterToolsHomeReducer/THEATER_TOOLS_HOME_SUCCESS";
export const THEATER_TOOLS_HOME_FAIL =
  "@theaterToolsHomeReducer/THEATER_TOOLS_HOME_FAIL";
