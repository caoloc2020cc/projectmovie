import * as ActionType from "./constants";

let initialState = {
  loading: false,
  data: null,
  err: null,
};

const theaterToolsHomeReducer = (state = initialState, action) => {
  switch (action.type) {
    case ActionType.THEATER_TOOLS_HOME_REQUEST:
      state.loading = true;
      state.data = null;
      state.err = null;
      return { ...state };
    case ActionType.THEATER_TOOLS_HOME_SUCCESS:
      console.log(action.payload, "scoob");
      state.loading = false;
      state.data = action.payload;
      state.err = null;
      return { ...state };
    case ActionType.THEATER_TOOLS_HOME_FAIL:
      state.loading = false;
      state.data = null;
      state.err = action.payload;
      return { ...state };

    default:
      return { ...state };
  }
};

export default theaterToolsHomeReducer;
