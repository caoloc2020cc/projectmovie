import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import Loader from "../../../components/Loader/Loader";
import { fetchListMovieHome } from "./../Slides/module/action";
import { actModalHomeSuccess } from "./../Modal/module/action";
import "./../../../assets/sass/layouts/slider.scss";
import FindFilm from "./FindFilm/index";
import LoaderHome from "../../../components/loaderHome";
class Slides extends Component {
  componentDidMount() {
    this.props.getSlideMovies();
  }

  renderSlidesMovie = () => {
    const { loading, data } = this.props;
    if (loading) {
      return <Loader />;
    } else if (data) {
      let slidesMovie = data.slice(0, 3);
      console.log(data);
      return (
        <>
          <div className="carousel-item active">
            <Link to={`/detail-movie/${slidesMovie[0].maPhim}`}>
              <img
                className="img-slides"
                src={slidesMovie[0].hinhAnh}
                alt="..."
              />
              <div className="shadow"></div>
            </Link>
            <button
              type="button"
              className="play__video"
              data-toggle="modal"
              data-target={"#" + slidesMovie[0].biDanh}
              onClick={() => this.props.getModalItem(slidesMovie[0])}
            >
              <img src="./images/play-video.png" alt />
            </button>
          </div>
          <div className="carousel-item">
            <Link to={`/detail-movie/${slidesMovie[1].maPhim}`}>
              <img
                className="img-slides"
                src={slidesMovie[1].hinhAnh}
                alt="..."
              />
              <div className="shadow"></div>
            </Link>
            <button
              type="button"
              className="play__video"
              data-toggle="modal"
              data-target={"#" + slidesMovie[1].biDanh}
              onClick={() => this.props.getModalItem(slidesMovie[1])}
            >
              <img src="./images/play-video.png" alt />
            </button>
          </div>
          <div className="carousel-item">
            <Link to={`/detail-movie/${slidesMovie[2].maPhim}`}>
              <img
                className="img-slides"
                src={slidesMovie[2].hinhAnh}
                alt="..."
              />
              <div className="shadow"></div>
            </Link>
            <button
              type="button"
              className="play__video"
              data-toggle="modal"
              data-target={"#" + slidesMovie[2].biDanh}
              onClick={() => this.props.getModalItem(slidesMovie[2])}
            >
              <img src="./images/play-video.png" alt />
            </button>
          </div>
        </>
      );
    }
  };

  render() {
    if (this.props.loading) {
      return <LoaderHome />;
    }
    return (
      <section className="slides">
        <div
          id="carouselExampleIndicators"
          className="carousel slide"
          data-ride="carousel"
        >
          <ol className="carousel-indicators">
            <li
              data-target="#carouselExampleIndicators"
              data-slide-to={0}
              className="active"
            />
            <li data-target="#carouselExampleIndicators" data-slide-to={1} />
            <li data-target="#carouselExampleIndicators" data-slide-to={2} />
          </ol>
          <div className="carousel-inner">{this.renderSlidesMovie()}</div>
          <a
            className="carousel-control-prev icon__slides"
            href="#carouselExampleIndicators"
            role="button"
            data-slide="prev"
          >
            <img src="./images/back-session.png" alt />
            <span className="sr-only">Previous</span>
          </a>
          <a
            className="carousel-control-next icon__slides"
            href="#carouselExampleIndicators"
            role="button"
            data-slide="next"
          >
            <img src="./images/next-session.png" alt />
            <span className="sr-only">Next</span>
          </a>
        </div>
        {/* <div className="select__menu">
          <div className="select__film">
            <div
              className="btn"
              id="Phim"
              data-toggle="dropdown"
              aria-haspopup="true"
              aria-expanded="false"
            >
              Phim
            </div>
            <div className="dropdown-menu" aria-labelledby="Phim">
              {this.renderMovieTools()}
            </div>
          </div>
          <TheaterTools />
        </div> */}
        <FindFilm dataMovie={this.props.data} />
      </section>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    loading: state.listMovieHomeReducer.loading,
    data: state.listMovieHomeReducer.data,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getSlideMovies: () => {
      dispatch(fetchListMovieHome());
    },
    getModalItem: (movie) => {
      dispatch(actModalHomeSuccess(movie));
    },
    //   getTheaterToolsMovie: (id) => {
    //     dispatch(fetchTheaterToolsHome(id));
    //   },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Slides);
