import * as ActionType from "./constants";

const initialState = {
  modal: null,
};

const modalHomeReducer = (state = initialState, action) => {
  switch (action.type) {
    case ActionType.MODAL_HOME_SUCCESS:
      state.modal = action.payload;

      return { ...state };
    default:
      return { ...state };
  }
};

export default modalHomeReducer;
