import * as ActionType from "./constants";
export const setMovie = (modal) => {
  return (dispatch) => dispatch(actModalHomeSuccess(modal));
};
export const actModalHomeSuccess = (modal) => {
  return {
    type: ActionType.MODAL_HOME_SUCCESS,
    payload: modal,
  };
};
