import React, { Component } from "react";
import { connect } from "react-redux";
import { setMovie } from "./module/action";
class Modal extends Component {
  // handleVideo = () => {
  //   document.getElementsByClassName(this.props.movie.biDanh)[0].src =
  //     document.getElementsByClassName(this.props.movie.biDanh)[0].src;
  // };
  render() {
    const { movie } = this.props;

    return movie ? (
      <div
        class="modal fade"
        id={movie.biDanh}
        tabindex="-1"
        role="dialog"
        aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true"
        onClick={() => this.props.getMovie(null)}
      >
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-body">
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
                onClick={() => this.props.getMovie(null)}
              >
                <img src="/images/close.png" alt />
              </button>
              <iframe
                className={movie.biDanh}
                width="800"
                height="500"
                src={movie.trailer}
                title="YouTube video player"
                frameborder="0"
                allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                allowfullscreen
              ></iframe>
            </div>
          </div>
        </div>
      </div>
    ) : (
      <></>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    movie: state.modalHomeReducer.modal,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    getMovie: (modal) => {
      dispatch(setMovie(modal));
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Modal);
